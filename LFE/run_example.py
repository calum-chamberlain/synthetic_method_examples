"""
    Example of synthetic template matched-filter detection for low-frequency
    earthquakes.

    :author:    Calum J. Chamberlain
    :date:      18/10/2017

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import glob
import numpy as np
from copy import deepcopy

from obspy import read_events, read, Stream, Trace, Catalog, UTCDateTime
from obspy.clients.fdsn import Client
from obspy.core.event import (
    FocalMechanism, NodalPlane, NodalPlanes, MomentTensor, Comment,
    ResourceIdentifier, Magnitude)

from eqcorrscan import Tribe, Template
from eqcorrscan.utils.correlate import pool_boy

from multiprocessing import Pool as ProcessPool

from utils.utilities import (
    _log_print, seis_sim, detect_in_data, axitra_sim, Capturing,
    predict_pick_times)
from utils.coordinates import Geographic, Location
from utils.plotting import plot_detection_rate, gmt_triple_plot
from utils.utilities import AXITRA_PATH


# Channel mapping
NZ_channels = {'SZ': 'HHZ', 'SN': 'HHN', 'SE': 'HHE', 'S1': 'HH1', 'S2': 'HH2',
               'HZ': 'HHZ', 'HN': 'HHN', 'HE': 'HHE', 'H1': 'HH1', 'H2': 'HH2'}
SAMBA_channels = {
    'SZ': 'EHZ', 'SN': 'EH1', 'SE': 'EH2', 'S1': 'EH1', 'S2': 'EH2',
    'SHZ': 'EHZ', 'SHN': 'EH1', 'SHE': 'EH2', "SH1": "EHZ", "SH2": "EH1",
    "SH3": "EH2"}
MAX_THREADS = 10


def generate_synth_templates(lowcut, highcut, samp_rate, filt_order,
                             prepick, length, phase_length, catalog,
                             process_len=7200):
    """Generate synthetic templates."""
    tribe = Tribe()
    for event in catalog:
        template = Template(
            name='_'.join(event.comments[0].text.split()),
            st=None, lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
            filt_order=filt_order, process_length=process_len, prepick=prepick,
            event=event)
        picks = {'P': [], 'S': []}
        for pick in event.picks:
            if pick.phase_hint in ['P', 'S']:
                picks[pick.phase_hint].append((
                    pick.waveform_id.station_code, pick.time,
                    pick.waveform_id.channel_code))
        stream = Stream()
        for station, p_time, channel in picks['P']:
            try:
                s_pick = [p for p in picks['S'] if p[0] == station][0]
                s_time = s_pick[1]
            except IndexError:
                continue
            sp_time = s_time - p_time
            z_data = seis_sim(
                sp=int(sp_time * samp_rate), flength=int(length * samp_rate),
                sine_samp=0.05)
            h_data = seis_sim(
                sp=int(sp_time * samp_rate), flength=int(length * samp_rate),
                amp_ratio=3, sine_samp=0.05)
            for data, chan in zip([z_data, h_data], [channel, s_pick[2]]):
                trace = Trace(data=data)
                trace.stats.station = station
                trace.stats.channel = chan
                if len(station) >= 4:
                    trace.stats.network = '9F'
                else:
                    trace.stats.network = 'NZ'
                    trace.stats.location = '10'
                trace.stats.sampling_rate = samp_rate
                trace.stats.starttime = p_time - (10 / samp_rate)
                # if chan == channel:
                trace.trim(starttime=p_time - (10 / samp_rate),
                           endtime=p_time - (10 / samp_rate) + phase_length)
                # else:
                #     trace.trim(starttime=s_time - (10 / samp_rate),
                #                endtime=s_time - (10 / samp_rate) + phase_length)
                stream += trace
        stream.filter('bandpass', freqmin=lowcut, freqmax=highcut,
                      corners=filt_order)
        if len(stream) != 0:
            template.st = stream
            tribe += template
    return tribe


def generate_synth_axitra_gf(lowcut, highcut, samp_rate, filt_order, prepick,
                             length, catalog, inventory, velocities,
                             verbose=False, parallel=False, process_len=7200):
    """Generate the synthetic templates from axitra data."""
    tribe = Tribe()
    if not parallel:
        for i, event in enumerate(catalog):
            template = _generate_synth_axitra_gf_single(
                lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
                filt_order=filt_order, prepick=prepick, length=length,
                event=event, inventory=inventory, velocities=velocities,
                verbose=verbose, i=i)
            if template is not None:
                tribe += template
    else:
        if len(catalog) > MAX_THREADS:
            threads = MAX_THREADS
        else:
            threads = len(catalog)
        with pool_boy(ProcessPool, threads) as pool:
            try:
                params = ((
                    lowcut, highcut, samp_rate, filt_order, prepick, length,
                    event, inventory, velocities, verbose, i)
                    for i, event in enumerate(catalog))
                results = [
                    pool.apply_async(_generate_synth_axitra_gf_single, param)
                    for param in params]
                templates = [res.get() for res in results]
                for template in templates:
                    if template is not None:
                        tribe.templates.append(template)
            except Exception as e:
                raise e
    for template in tribe:
        template.process_length = process_len
    if len(tribe) == 0:
        raise IndexError("No templates created, Wah.")
    return tribe


def _generate_synth_axitra_gf_single(lowcut, highcut, samp_rate, filt_order,
                                     prepick, length, event, inventory,
                                     velocities, verbose=False, i=0):
    """
    Generate a single axitra derived synthetic template.
    """
    if len(event.focal_mechanisms) == 0:
        if verbose:
            print("Cannot generate a synthetic, we have no moment tensor!")
            print(event)
        return Template()
    syn_st = axitra_sim(
        event=event, inventory=inventory, velocities=velocities,
        path=AXITRA_PATH, verbose=verbose, offset=-9,
        wavelet_period=0.1, index=i, apply_response=False)
    single_tribe = Tribe().construct(
        method='from_meta_file', lowcut=lowcut, highcut=highcut,
        samp_rate=samp_rate, filt_order=filt_order, prepick=prepick,
        st=syn_st, swin='P_all', meta_file=Catalog([event]), length=length,
        debug=0, all_horiz=True)
    try:
        single_tribe[0].name = '_'.join(event.comments[0].text.split())
    except IndexError:
        print("Event origin {0} not within time-span of data, starting at "
              "{1}".format(event.origins[0].time, syn_st[0].stats.starttime))
        return
    return single_tribe[0]


def generate_grid_tribes(master, x_range, y_range, depth_range, lowcut,
                         highcut, samp_rate, filt_order, prepick, length,
                         inventory, velocities, verbose, synth_length=50,
                         orientation=0.):
    """
    Generate a grid of templates around a master location.
    """
    catalog = Catalog()
    master_origin = master.preferred_origin() or master.origins[0]
    master_loc = Geographic(latitude=master_origin.latitude,
                            longitude=master_origin.longitude,
                            depth=master_origin.depth / -1000)
    # Check that no positive depths occur - taup will error
    _depth_range = []
    for depth_var in depth_range:
        if master_loc.depth + depth_var >= 0:
            print("Positive depth found, not using this")
        else:
            _depth_range.append(depth_var)
    depth_range = _depth_range
    x_y_z = ((x_var, y_var, depth_var)
             for depth_var in depth_range for y_var in y_range
             for x_var in x_range)
    n_variations = len(x_range) * len(y_range) * len(depth_range)
    if n_variations > MAX_THREADS:
        threads = MAX_THREADS
    else:
        threads = n_variations
    with pool_boy(ProcessPool, threads) as pool:
        try:
            results = [
                pool.apply_async(_shift_event_location,
                                 (master, master_origin, master_loc,
                                  inventory, x_var, y_var, depth_var,
                                  orientation))
                for x_var, y_var, depth_var in x_y_z]
            catalog.events = [res.get() for res in results]
        except Exception as e:
            raise e
    tribes = {
        'synthetic': generate_synth_templates(
            lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
            filt_order=filt_order, prepick=prepick, length=synth_length,
            phase_length=length, catalog=catalog),
        'gf': generate_synth_axitra_gf(
            lowcut, highcut, samp_rate, filt_order, prepick, length,
            catalog=catalog, inventory=inventory, velocities=velocities,
            parallel=True)}
    return tribes


def _shift_event_location(master, master_origin, master_loc, inventory,
                          x_var, y_var, depth_var, orientation,
                          first_n_stations=7):
    # Convert x and y to long and lat
    altered_loc = Location(x=x_var, y=y_var, z=depth_var,
                           origin=master_loc, dip=90, strike=orientation)
    altered_loc = altered_loc.to_geographic()
    altered_ev = master.copy()
    altered_ev.origins = [master_origin.copy()]
    altered_ev.origins[0].resource_id = ResourceIdentifier()
    altered_ev.origins[0].latitude = altered_loc.latitude
    altered_ev.origins[0].longitude = altered_loc.longitude
    altered_ev.origins[0].depth = altered_loc.depth * -1000
    altered_ev.preferred_origin_id = \
        altered_ev.origins[0].resource_id
    altered_ev = predict_pick_times(
        event=altered_ev, inventory=inventory, only_picked=False)
    if first_n_stations is not None:
        P_picks = [p for p in altered_ev.picks if p.phase_hint == "P"]
        P_picks.sort(key=lambda p: p.time)
        used_stations = [
            p.waveform_id.station_code for p in P_picks[0:first_n_stations]]
        used_picks = []
        for pick in altered_ev.picks:
            if pick.waveform_id.station_code in used_stations:
                used_picks.append(pick)
        altered_ev.picks = used_picks
    x_pol, y_pol, depth_pol = ("pos", "pos", "pos")
    if x_var < 0:
        x_pol = "neg"
    if y_var < 0:
        y_pol = "neg"
    if depth_var < 0:
        depth_pol = "neg"
    altered_ev.comments = [Comment(
        text="moved by x {0}{1}km y {2}{3}km depth "
             "{4}{5}km".format(
                x_pol, '_'.join(str(abs(x_var)).split('.')),
                y_pol, '_'.join(str(abs(y_var)).split('.')),
                depth_pol, '_'.join(str(abs(depth_var)).split('.'))))]
    altered_ev.resource_id = ResourceIdentifier()
    return altered_ev


def run(template_plot=False, verbose=False):
    directory = os.path.abspath(os.path.dirname(__file__))
    _log_print(("Running " + os.path.split(directory)[-1] +
                " example").center(80), "=")
    lowcut, highcut, samp_rate, filt_order, prepick, length = (
        1, 5, 25, 4, 0.5, 15)
    t1 = UTCDateTime(2010, 8, 20, 4, 30)
    t2 = t1 + 7200
    with Capturing() as _:
        sfiles = glob.glob(os.path.join(
            directory, "data", "previous_work", "template_picks", "*"))
        catalog = Catalog()
        for sfile in sfiles:
            event = read_events(sfile)[0]
            event.resource_id.id = sfile.split('/')[-1].split('_')[0]
            catalog.append(event)

    # The data that the picks were made on were not properly named, we need
    # to map to the correct stations and channels
    for event in catalog:
        for pick in event.picks:
            pick.waveform_id.station_code = \
                pick.waveform_id.station_code.upper()
            if pick.waveform_id.station_code in ["POCR", "WHAT"]:
                pick.waveform_id.station_code += "2"
                pick.waveform_id.location_code = '10'
            if len(pick.waveform_id.station_code) == 3:
                pick.waveform_id.network_code = 'NZ'
                pick.waveform_id.location_code = '10'
                pick.waveform_id.channel_code = \
                    NZ_channels[pick.waveform_id.channel_code]
            else:
                pick.waveform_id.network_code = '9F'
                pick.waveform_id.channel_code = \
                    SAMBA_channels[pick.waveform_id.channel_code]
    for event in catalog:
        phase_picks = []
        for pick in event.picks:
            if pick.phase_hint in ['P', 'S']:
                phase_picks.append(pick)
        event.picks = phase_picks
    # Add in approximate moment-tensor information from Baratin et al., 2018's
    # focal mechanism for family 1.
    fm = FocalMechanism(nodal_planes=NodalPlanes(
        nodal_plane_1=NodalPlane(strike=52.0, dip=66.0, rake=141.0)),
        moment_tensor=MomentTensor(scalar_moment=1e20))
    for event in catalog:
        _fm = deepcopy(fm)
        _fm.triggering_origin_id = event.origins[0].resource_id
        event.focal_mechanisms.append(_fm)
    # Read the SAMBA data from disk
    data = read(os.path.join(
        directory, "data", "continuous_data", "Y2010_R232", "*"))
    for tr in data:
        if tr.stats.station in ["POCR2", "WHAT2"]:
            tr.stats.location = '10'
    data.merge()
    data.trim(t1, t2)
    # Download the GeoNet data
    client = Client("GEONET")
    stations = ['FOZ', 'JCZ', 'RPZ', 'WVZ']
    bulk = []
    for station in stations:
        bulk.append(('NZ', station, '10', 'EH?', t1 - 200, t2 + 200))
        bulk.append(('NZ', station, '10', 'HH?', t1 - 200, t2 + 200))
    data += client.get_waveforms_bulk(bulk=bulk)
    data.merge()
    data.trim(t1, t2)
    for tr in data:
        if tr.stats.npts > int((t2 - t1) * tr.stats.sampling_rate):
            tr.data = tr.data[0: int((t2 - t1) * tr.stats.sampling_rate)]
        if tr.stats.network == 'AF':
            tr.stats.network = '9F'
            tr.stats.channel = SAMBA_channels[tr.stats.channel]
    # Get the station information appropriate for the continuous data
    bulk = []
    for tr in data.select(network="NZ"):
        bulk.append((tr.stats.network, tr.stats.station, tr.stats.location,
                     tr.stats.channel, tr.stats.starttime, tr.stats.endtime))
    inv = client.get_stations_bulk(bulk=bulk, level="response")
    client = Client("IRIS")
    inv += client.get_stations(network="9F", starttime=data[0].stats.starttime,
                               endtime=data[0].stats.endtime, level="response")
    _inv = deepcopy(inv)
    _inv.networks = []
    for network in inv:
        _net = network.copy()
        _net.stations = []
        for station in network:
            if station.code in [tr.stats.station for tr in data]:
                _net.stations.append(station)
        if len(_net.stations) > 0:
            _inv.networks.append(_net)
    inv = _inv
    for tr in data:
        tr.split().detrend().merge().detrend()
        tr.remove_response(
            inventory=inv, pre_filt=[0.01, 0.05, 45, 50], zero_mean=True,
            taper=True, taper_fraction=0.05)
        # apply constant gain to maintain floating point accuracy
        tr.data *= 1e6
    # Geonet returns all channels when level=response
    inv = inv.select(starttime=catalog[0].origins[0].time.date,
                     endtime=(catalog[0].origins[0].time + 86400).date)
    # Velocities are: (depth (m), Vp (m/s), Vs (m/s), rho, Qp, Qs)
    # Construct the velocities - These are taken from table 1 of Ristau 2008,
    # "Implementation of Routine Regional Moment Tensor Analysis in New
    # Zealand". Velocities used here are those for "South Island" in table 1
    velocities = [
        (3000., 5000., 3000.,  2670., 400., 200.),
        (12000., 6000., 3600.,  2700., 400., 200.),
        (27000., 6500., 3700.,  2720., 400., 200.),
        (39000., 7500., 4300.,  2750., 400., 200.),
        (58000., 8100., 4600.,  3040., 400., 200.)]
    # Generate the tribes
    tribes = generate_grid_tribes(
        master=catalog[0], y_range=np.arange(-60, 61, 2.5),
        x_range=np.arange(-30, 31, 2.5), depth_range=np.arange(-15, 11, 2),
        orientation=57.26, lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
        filt_order=filt_order, prepick=prepick, length=length,
        inventory=inv, velocities=velocities, verbose=verbose, synth_length=50)
    # Hack
    for tribe in tribes.values():
        for template in tribe:
            station_list = []
            id_list = []
            for tr in template.st:
                if tr.id in id_list:
                    template.st.remove(tr)
                    continue
                if len(tr) > length * samp_rate:
                    tr.data = tr.data[0:length * samp_rate]
                if len(tr) < .75 * length * samp_rate:
                    template.st.remove(tr)
                    continue
                id_list.append(tr.id)
                station_list.append(tr.stats.station)
            if len(set(station_list)) < 3:
                tribe.remove(template)
    if template_plot:
        catalog.plot(projection='local', resolution='h')
        for key in tribes.keys():
            for template in tribes[key]:
                print("Plotting template {0}".format(template.name))
                template.st.plot(size=(800, 600), equal_scale=False,
                                 title=template.name)
    for key in tribes.keys():
        _log_print("Writing out tribe: {0}".format(key), ".")
        tribes[key].write(directory + '/templates/' + key)
    parties = {}
    for key in tribes.keys():
        _log_print("Detecting for %s tribe" % key, ".")
        party = detect_in_data(
            tribes[key], data=data, verbose=verbose, mad_mult=9.0, cores=20,
            parallel_process=True, daylong=False)
        party.write(
            filename=os.path.join(
                directory, 'data', 'detections', key + '_tribe'))
        parties.update({key: party})
    filtered_data = data.split().merge(fill_value='interpolate').copy().filter(
        'bandpass', freqmin=2, freqmax=10)
    # for party in parties.values():
    #     snr_threshold(party, 1.5, data, window=(-2, 10))
    _log_print("Plotting", ".")
    plot_templates = {
        'gf': tribes['gf'][0],
        'synthetic': tribes['synthetic'][0]}
    for key, party in parties.items():
        try:
            party.decluster(20)
        except IndexError:
            print("{0} detections for party {1} were not declustered".format(
                len(party), key))
    for party in parties.values():
        party.rethreshold(10)
        _fams = []
        for family in party.families:
            if len(family) != 0:
                _fams.append(family)
        party.families = _fams
    detect_catalog = Catalog()
    for party in parties.values():
        if len(party) == 0:
            continue
        for family in party:
            if len(family) != 0:
                detect_catalog.append(family.template.event)
    for event in detect_catalog:
        event.magnitudes = [Magnitude(mag=4.0)]
        event.magnitudes[0].resource_id = ResourceIdentifier()
        event.preferred_magnitude_id = event.magnitudes[0].resource_id
    fig = gmt_triple_plot(
        # parties=parties, catalog=catalog, station="GOVA", channel="EH2",
        parties=parties, catalog=catalog, station="MTFO", channel="EH1",
        data=filtered_data, plot_len=15, inventory=inv, pre_det=3,
        template_catalog=detect_catalog, templates=plot_templates,
        label_size=11, label_offset=5.5, scale_length=40, 
        region_pad=0.0, map_region=[169.3, 170.8, -44.6, -43.05], 
        template_offset=4.9, template_height=3.1, fm=[
            fm.nodal_planes.nodal_plane_1.strike,
            fm.nodal_planes.nodal_plane_1.dip,
            fm.nodal_planes.nodal_plane_1.rake])
    fig.savefig("{0}{1}plots{1}{2}_gmt_triple_plot.eps".format(
        directory, os.path.sep, os.path.split(directory)[-1]))
    # read in the appropriate tremor detection times from Wech's work.
    tremor_times = []
    with open(directory + "/data/previous_work/trems_forCaro.txt", "r") as f:
        for line in f:
            line = line.split()
            try:
                trem_start = "T".join([line[0], line[1]])
            except IndexError:
                continue  # Empty line
            # trem_end = "T".join([line[0], line[2]])
            trem_start = UTCDateTime.strptime(trem_start, "%Y-%m-%dT%H:%M:%S")
            # trem_end = UTCDateTime.strptime(trem_end, "%Y-%m-%dT%H:%M:%S")
            if data[0].stats.starttime < trem_start < data[0].stats.endtime:
                tremor_times.append(trem_start)
    fig = plot_detection_rate(
        parties=parties, data=data, catalog_times=tremor_times,
        station="COVA", channel="EH1", gain=1e6)
    fig.savefig("{0}{1}plots{1}{2}_detection_rate.eps".format(
        directory, os.path.sep, os.path.split(directory)[-1]))


if __name__ == '__main__':
    run()

"""
Script to run the other days on which tremor was identified. Uses local
data, so will not work outside of VUW.
"""
import os
import sys
import glob
import logging

from copy import deepcopy
from obspy import UTCDateTime, read, read_events
from obspy.clients.fdsn import Client
from obspy.core.event import (
    FocalMechanism, NodalPlane, NodalPlanes, MomentTensor, Comment,
    ResourceIdentifier, Magnitude, Catalog)

from eqcorrscan import Tribe

directory = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, os.path.dirname(directory))

from utils.utilities import _log_print, Capturing, detect_in_data
from utils.plotting import plot_detection_rate, gmt_triple_plot

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s")
Logger = logging.getLogger("generate_templates.__main__")

SAMBA_data = (
    "/Volumes/GeoPhysics_09/users-data/chambeca/SAMBA_archive/day_volumes_S")
NZ_channels = {'SZ': 'HHZ', 'SN': 'HHN', 'SE': 'HHE', 'S1': 'HH1', 'S2': 'HH2',
               'HZ': 'HHZ', 'HN': 'HHN', 'HE': 'HHE', 'H1': 'HH1', 'H2': 'HH2'}
SAMBA_channels = {
    'SZ': 'EHZ', 'SN': 'EH1', 'SE': 'EH2', 'S1': 'EH1', 'S2': 'EH2',
    'SHZ': 'EHZ', 'SHN': 'EH1', 'SHE': 'EH2', "SH1": "EHZ", "SH2": "EH1",
    "SH3": "EH2"}
MAX_THREADS = 10

def detect_and_plot(tribes, inv, data, fm, catalog, verbose=True):
    Logger.info("Removing response")
    for tr in data:
        Logger.info("Working on {0}".format(tr.id))
        Logger.info("Trace is {0} data points long".format(tr.stats.npts))
        tr.split().detrend().merge().detrend()
        tr.remove_response(
            inventory=inv, pre_filt=[0.01, 0.05, 45, 50], zero_mean=True,
            taper=True, taper_fraction=0.05)
        # apply constant gain to maintain floating point accuracy
        tr.data *= 1e6
    # Geonet returns all channels when level=response
    inv = inv.select(starttime=catalog[0].origins[0].time.date,
                     endtime=(catalog[0].origins[0].time + 86400).date)
    parties = {}
    for key in tribes.keys():
        Logger.info("Detecting for {0} tribe".format(key))
        party = detect_in_data(
            tribes[key], data=data, verbose=verbose, mad_mult=10.0, cores=20,
            parallel_process=False, daylong=False)
        party.write(
            filename=os.path.join(
                directory, 'data', 'detections',
                key + '_tribe_' + data[0].stats.starttime.strftime(
                    "%Y%m%dT%H:%M:%S")))
        parties.update({key: party})
    filtered_data = data.split().merge(fill_value='interpolate').copy().filter(
        'bandpass', freqmin=2, freqmax=10)
    # for party in parties.values():
    #     snr_threshold(party, 1.5, data, window=(-2, 10))
    Logger.info("Plotting", ".")
    plot_templates = {
        'gf': tribes['gf'][0],
        'synthetic': tribes['synthetic'][0]}
    for key, party in parties.items():
        try:
            party.decluster(20)
        except IndexError:
            print("{0} detections for party {1} were not declustered".format(
                len(party), key))
    detect_catalog = Catalog([f.template.event for party in parties.values()
                              for f in party if len(f.detections) > 0])
    for event in detect_catalog:
        event.magnitudes = [Magnitude(mag=4.0)]
        event.magnitudes[0].resource_id = ResourceIdentifier()
        event.preferred_magnitude_id = event.magnitudes[0].resource_id
    fig = gmt_triple_plot(
        # parties=parties, catalog=catalog, station="GOVA", channel="EH2",
        parties=parties, catalog=catalog, station="MTFO", channel="EH1",
        data=filtered_data, plot_len=15, inventory=inv, pre_det=3,
        template_catalog=detect_catalog, templates=plot_templates,
        label_size=11, label_offset=2.5, fm=[
            fm.nodal_planes.nodal_plane_1.strike,
            fm.nodal_planes.nodal_plane_1.dip,
            fm.nodal_planes.nodal_plane_1.rake])
    fig.savefig("{0}{1}plots{1}{2}_{3}_gmt_triple_plot.eps".format(
        directory, os.path.sep, os.path.split(directory)[-1]),
        data[0].stats.starttime)
    # read in the appropriate tremor detection times from Wech's work.
    tremor_times = []
    with open(directory + "/data/previous_work/trems_forCaro.txt", "r") as f:
        for line in f:
            line = line.split()
            try:
                trem_start = "T".join([line[0], line[1]])
            except IndexError:
                continue  # Empty line
            # trem_end = "T".join([line[0], line[2]])
            trem_start = UTCDateTime.strptime(trem_start, "%Y-%m-%dT%H:%M:%S")
            # trem_end = UTCDateTime.strptime(trem_end, "%Y-%m-%dT%H:%M:%S")
            if data[0].stats.starttime < trem_start < data[0].stats.endtime:
                tremor_times.append(trem_start)
    fig = plot_detection_rate(
        parties=parties, data=data, catalog_times=tremor_times,
        station="COVA", channel="EH1")
    fig.savefig("{0}{1}plots{1}{2}_{3}_detection_rate.eps".format(
        directory, os.path.sep, os.path.split(directory)[-1]),
        data[0].stats.starttime)


if __name__ == "__main__":
    tremor_days = [
        (UTCDateTime(2009, 5, 12, 15), UTCDateTime(2009, 5, 13)),
        (UTCDateTime(2009, 7, 14, 16), UTCDateTime(2009, 7, 14, 18)),
        (UTCDateTime(2009, 7, 15, 7), UTCDateTime(2009, 7, 15, 9)),
        (UTCDateTime(2009, 11, 15, 10), UTCDateTime(2009, 11, 15, 11)),
        (UTCDateTime(2010, 7, 5, 13), UTCDateTime(2010, 7, 5, 14)),
        (UTCDateTime(2010, 7, 5, 17), UTCDateTime(2010, 7, 5, 18)),
        (UTCDateTime(2010, 7, 14, 7), UTCDateTime(2010, 7, 14, 9)),
        (UTCDateTime(2010, 8, 20, 4), UTCDateTime(2010, 8, 20, 6)),
        (UTCDateTime(2010, 8, 31, 16), UTCDateTime(2010, 8, 31, 17)),
        (UTCDateTime(2010, 10, 5, 8), UTCDateTime(2010, 10, 5, 9)),
        (UTCDateTime(2011, 8, 3, 13), UTCDateTime(2011, 8, 3, 14)),
        (UTCDateTime(2011, 9, 2, 18), UTCDateTime(2011, 9, 2, 19)),
        (UTCDateTime(2011, 9, 4, 17), UTCDateTime(2011, 9, 4, 18))]
    Logger.info("Reading Tribes")
    tribes = {
        'gf': Tribe().read(
            '{0}/templates/7_stations_gf.tgz'.format(directory)),
        'synthetic': Tribe().read(
            '{0}/templates/7_stations_synthetic.tgz'.format(directory))}
    # Change process-length to 1 hour to conserve memory
    # for tribe in tribes.values():
    #     for template in tribe:
    #         template.process_length = 3600.0
    Logger.info("Read in tribes")

    with Capturing() as _:
        sfiles = glob.glob(os.path.join(
            directory, "data", "previous_work", "template_picks", "*"))
        catalog = Catalog()
        for sfile in sfiles:
            event = read_events(sfile)[0]
            event.resource_id.id = sfile.split('/')[-1].split('_')[0]
            catalog.append(event)

    # The data that the picks were made on were not properly named, we need
    # to map to the correct stations and channels
    for event in catalog:
        for pick in event.picks:
            pick.waveform_id.station_code = (
                pick.waveform_id.station_code.upper())
            if pick.waveform_id.station_code in ["POCR", "WHAT"]:
                pick.waveform_id.station_code += "2"
                pick.waveform_id.location_code = '10'
            if len(pick.waveform_id.station_code) == 3:
                pick.waveform_id.network_code = 'NZ'
                pick.waveform_id.location_code = '10'
                pick.waveform_id.channel_code = (
                    NZ_channels[pick.waveform_id.channel_code])
            else:
                pick.waveform_id.network_code = '9F'
                pick.waveform_id.channel_code = (
                    SAMBA_channels[pick.waveform_id.channel_code])
    for event in catalog:
        phase_picks = []
        for pick in event.picks:
            if pick.phase_hint in ['P', 'S']:
                phase_picks.append(pick)
        event.picks = phase_picks
    # Add in approximate moment-tensor information from Baratin et al., 2018's
    # focal mechanism for family 1.
    fm = FocalMechanism(nodal_planes=NodalPlanes(
        nodal_plane_1=NodalPlane(strike=52.0, dip=66.0, rake=141.0)),
        moment_tensor=MomentTensor(scalar_moment=1e20))
    for event in catalog:
        _fm = deepcopy(fm)
        _fm.triggering_origin_id = event.origins[0].resource_id
        event.focal_mechanisms.append(_fm)
    for t1, t2 in tremor_days:
        Logger.info("Working between {0} {1}".format(t1, t2))
        # Read the SAMBA data from disk
        data = read(os.path.join(SAMBA_data, t1.strftime("Y%Y/R%j.01"), "*"))
        for tr in data:
            if tr.stats.station in ["POCR2", "WHAT2"]:
                tr.stats.location = '10'
        # Remove POCR2 surface sensor
        for tr in data:
            if tr.stats.station == "POCR2" and\
              tr.stats.channel in ['SHZ', 'SHN', 'SHE']:
                data.remove(tr)
        data.merge()
        data.trim(t1, t2)
        # Download the GeoNet data
        client = Client("GEONET")
        stations = ['FOZ', 'JCZ', 'RPZ', 'WVZ']
        bulk = []
        for station in stations:
            bulk.append(('NZ', station, '10', 'EH?', t1 - 200, t2 + 200))
            bulk.append(('NZ', station, '10', 'HH?', t1 - 200, t2 + 200))
        data += client.get_waveforms_bulk(bulk=bulk)
        data.merge()
        data.trim(t1, t2)
        for tr in data:
            if tr.stats.npts > int((t2 - t1) * tr.stats.sampling_rate):
                tr.data = tr.data[0: int((t2 - t1) * tr.stats.sampling_rate)]
            if tr.stats.network == 'AF':
                tr.stats.network = '9F'
                tr.stats.channel = SAMBA_channels[tr.stats.channel]
        # Get the station information appropriate for the continuous data
        bulk = []
        for tr in data.select(network="NZ"):
            bulk.append(
                (tr.stats.network, tr.stats.station, tr.stats.location,
                 tr.stats.channel, tr.stats.starttime, tr.stats.endtime))
        inv = client.get_stations_bulk(bulk=bulk, level="response")
        client = Client("IRIS")
        inv += client.get_stations(
            network="9F", starttime=data[0].stats.starttime,
            endtime=data[0].stats.endtime, level="response")
        _inv = deepcopy(inv)
        _inv.networks = []
        for network in inv:
            _net = network.copy()
            _net.stations = []
            for station in network:
                if station.code in [tr.stats.station for tr in data]:
                    _net.stations.append(station)
            if len(_net.stations) > 0:
                _inv.networks.append(_net)
        detect_and_plot(
            tribes=tribes, inv=inv, data=data, fm=fm, catalog=catalog)

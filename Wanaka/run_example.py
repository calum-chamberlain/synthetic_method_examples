"""
    Example of synthetic template matched-filter detection for earthquake
    aftershocks.

    :author:    Calum J. Chamberlain
    :data:      18/10/2017

    Licence:
    --------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import os
import glob
import warnings

from copy import deepcopy
from obspy import (
    read, read_events, Catalog, Stream, Trace, read_inventory, UTCDateTime)
from obspy.clients.fdsn import Client
from obspy.clients.fdsn.client import FDSNNoDataException
from obspy.core.event import (
    FocalMechanism, NodalPlane, NodalPlanes, MomentTensor, Magnitude)
from eqcorrscan.core.match_filter import Tribe, Template, Party, Family
from eqcorrscan.utils.correlate import pool_boy
from eqcorrscan.utils.trigger import TriggerParameters, network_trigger
from multiprocessing import cpu_count, Pool as ProcessPool

from utils.utilities import (
    detect_in_data, snr_threshold, _log_print, seis_sim, axitra_sim,
    party_to_tribe)
from utils.plotting import gmt_triple_plot, overlain_plot, plot_detection_rate

from utils.utilities import AXITRA_PATH

COSA_STATIONS = [
    'HAAS', 'HUVA', 'NOBU', 'KING', 'ASPR', 'MORV', 'STBA', 'TEPE']
STRONG_STATIONS = ['MOSS', 'NSBS', 'TAFS', 'TWAS']
MAX_THREADS = 10


def generate_synth_axitra_gf(lowcut, highcut, samp_rate, filt_order, prepick,
                             length, catalog, inventory, velocities,
                             verbose=False, parallel=False, process_len=86400):
    """Generate the synthetic templates from axitra data."""
    tribe = Tribe()
    if not parallel:
        for i, event in enumerate(catalog):
            template = _generate_synth_axitra_gf_single(
                lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
                filt_order=filt_order, prepick=prepick, length=length,
                event=event, inventory=inventory, velocities=velocities,
                verbose=verbose, i=i)
            tribe += template
    else:
        if len(catalog) > MAX_THREADS:
            threads = MAX_THREADS
        else:
            threads = len(catalog)
        with pool_boy(ProcessPool, threads) as pool:
            try:
                params = ((
                    lowcut, highcut, samp_rate, filt_order, prepick, length,
                    event, inventory, velocities, verbose, i)
                    for i, event in enumerate(catalog))
                results = [
                    pool.apply_async(_generate_synth_axitra_gf_single, param)
                    for param in params]
                tribe.templates = [res.get() for res in results]
            except Exception as e:
                raise e
    for template in tribe:
        template.process_length = process_len
    if len(tribe) == 0:
        raise IndexError("No templates created, Wah.")
    return tribe


def _generate_synth_axitra_gf_single(lowcut, highcut, samp_rate, filt_order,
                                     prepick, length, event, inventory,
                                     velocities, verbose=False, i=0):
    """
    Generate a single axitra derived synthetic template.
    """
    if len(event.focal_mechanisms) == 0:
        if verbose:
            print("Cannot generate a synthetic, we have no moment tensor!")
            print(event)
        return Template()
    syn_st = axitra_sim(
        event=event, inventory=inventory, velocities=velocities,
        path=AXITRA_PATH, verbose=verbose, offset=-9.75,
        wavelet_period=0.1, index=i)
    # Nearby stations have strange signals
    # picks_to_use = []
    # for pick in event.picks:
    #     if pick.waveform_id.station_code not in ['TWAS']:
    #         picks_to_use.append(pick)
    # event.picks = picks_to_use
    single_tribe = Tribe().construct(
        method='from_meta_file', lowcut=lowcut, highcut=highcut,
        samp_rate=samp_rate, filt_order=filt_order, prepick=prepick,
        st=syn_st, swin='all', meta_file=Catalog([event]), length=length,
        debug=0, all_horiz=True)
    single_tribe[0].name = '_'.join(event.comments[0].text.split())
    return single_tribe[0]


def generate_synth_templates(lowcut, highcut, samp_rate, filt_order, prepick,
                             length, catalog):
    """Generate synthetic templates."""
    tribe = Tribe()
    for event in catalog:
        template = Template(
            name=str(event.origins[0].time.strftime('%Y_%m_%dt%H_%M_%S')),
            st=None, lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
            filt_order=filt_order, process_length=86400, prepick=prepick,
            event=event)
        picks = {'P': [], 'S': []}
        for pick in event.picks:
            if pick.phase_hint in ['P', 'S']:
                picks[pick.phase_hint].append((
                    pick.waveform_id.station_code, pick.time,
                    pick.waveform_id.channel_code))
        stream = Stream()
        for station, p_time, channel in picks['P']:
            try:
                s_pick = [p for p in picks['S'] if p[0] == station][0]
                s_time = s_pick[1]
            except IndexError:
                continue
            sp_time = s_time - p_time
            Z_data = seis_sim(
                sp=int(sp_time * samp_rate), flength=int(length * samp_rate))
            H_data = seis_sim(
                sp=int(sp_time * samp_rate), flength=int(length * samp_rate),
                amp_ratio=3)
            for data, chan in zip([Z_data, H_data], [channel, s_pick[2]]):
                trace = Trace(data=data)
                trace.stats.station = station
                trace.stats.channel = chan
                if station in COSA_STATIONS:
                    trace.stats.location = '10'
                    trace.stats.network = 'CO'
                elif station in STRONG_STATIONS:
                    trace.stats.location = '20'
                    trace.stats.network = 'NZ'
                else:
                    trace.stats.network = 'NZ'
                    trace.stats.location = '10'
                trace.stats.sampling_rate = samp_rate
                trace.stats.starttime = p_time - (10 / samp_rate)
                stream += trace
        stream.filter('bandpass', freqmin=lowcut, freqmax=highcut,
                      corners=filt_order)
        template.st = stream
        tribe += template
    return tribe


def generate_real_templates(lowcut, highcut, samp_rate, filt_order, prepick,
                            length, data, catalog_file):
    """Generate the real templates."""
    tribe = Tribe().construct(
        method='from_meta_file', lowcut=lowcut, highcut=highcut,
        samp_rate=samp_rate, filt_order=filt_order, prepick=prepick, st=data,
        swin='all', meta_file=catalog_file, length=length, debug=0)
    return tribe


def detect_and_plot(tribes, directory, catalog, data, inv, verbose, lowcut,
                    highcut, plot_templates, snr_thresh=0, iteration=1,
                    template_catalog=None):
    parties = {}
    for key in tribes.keys():
        _log_print("Detecting for %s tribe" % key, ".")
        tribes[key].write(
            directory + '/templates/' + key + "_iter_{0}".format(iteration))
        party = detect_in_data(
            tribes[key], data=data, verbose=verbose, mad_mult=9, cores=10)
        party.write(filename=os.path.join(
            directory, 'data', 'detections',
            key + '_tribe_iter_{0}'.format(iteration)))
        parties.update({key: party})
    filtered_data = data.merge(fill_value='interpolate').filter(
        'bandpass', freqmin=lowcut, freqmax=highcut)
    d_start = UTCDateTime(2015, 5, 4)
    chunk_size = 15 * 60  # Plot chunk size in seconds
    while d_start <= UTCDateTime(2015, 5, 4, 23):
        print("Plotting for start-time {0}".format(d_start))
        d_end = d_start + chunk_size
        plot_parties = {}
        for key, party in parties.items():
            plot_party = Party()
            for family in party:
                plot_family = Family(template=family.template)
                hours_detections = []
                for detection in family:
                    if d_start < detection.detect_time < d_end:
                        hours_detections.append(detection)
                plot_family.detections = hours_detections
                plot_party += plot_family
            plot_parties.update({key: plot_party})
        _log_print("Plotting", ".")
        try:
            large_catalog = client.get_events(
                starttime=d_start, endtime=d_end, minlongitude=168.55087,
                minlatitude=-44.80328, maxlongitude=169.13452,
                maxlatitude=-44.3022)
        except FDSNNoDataException:
            large_catalog = Catalog()
        if len(plot_parties['gf']) + len(plot_parties['synthetic']) + len(plot_parties['real']) == 0:
            print("No detections for this hour, skipping plotting")
            d_start += chunk_size
            continue
        fig = gmt_triple_plot(
            parties=plot_parties, catalog=large_catalog, station="HUVA",
            channel="HHZ", data=filtered_data, plot_len=10, inventory=inv,
            label_offset=3.7, template_catalog=catalog, gap=20, catalog_diff=5,
            templates=plot_templates, label_size=10,
            region_pad=0.1, fm_pad=0.35, scale_length=40, tick_spacing=0.5,
            overview_region=[165.8, 175, -47.2, -40],
            map_region=[168., 170., -45.25, -43.8], 
            zoom_region=[168.8, 168.9, -44.58, -44.51],
            zoom_scale_length=4, template_height=3.8, template_offset=4.1,
            fm=[fm.nodal_planes.nodal_plane_1.strike,
                fm.nodal_planes.nodal_plane_1.dip,
                fm.nodal_planes.nodal_plane_1.rake])
        fig.savefig("{0}{1}plots{1}{2}_gmt_triple_plot_{3}_iter-{4}.eps".format(
            directory, os.path.sep, os.path.split(directory)[-1], d_start, iteration))
        d_start += chunk_size
    return parties


def cosa_network_trigger(st, ews_params=False):
    """
    Use coincidence trigger with sta/lta to detect.
    """
    if not ews_params:
        parameters = []
        for tr in st:
            parameters.append(TriggerParameters(
                {'station': tr.stats.station, 'channel': tr.stats.channel,
                'sta_len': 0.2, 'lta_len': 10.0, 'thr_on': 10.0,
                'thr_off': 7.0, 'lowcut': 2.0, 'highcut': 15.0}))
        triggers = network_trigger(
            st=st, parameters=parameters, thr_coincidence_sum=5, moveout=30,
            max_trigger_length=20, despike=False)
    else:
        parameters = []
        for tr in st:
            parameters.append(TriggerParameters(
                {'station': tr.stats.station, 'channel': tr.stats.channel,
                'sta_len': 30, 'lta_len': 700.0, 'thr_on': 1.0,
                'thr_off': 0.5, 'lowcut': 2.0, 'highcut': 15.0}))
        triggers = network_trigger(
            st=st, parameters=parameters, thr_coincidence_sum=5, moveout=30,
            max_trigger_length=200, despike=False)
    return triggers


def run(template_plot=False, verbose=False):
    directory = os.path.abspath(os.path.dirname(__file__))
    _log_print(("Running " + os.path.split(directory)[-1] +
                " example").center(80), "=")
    lowcut, highcut, samp_rate, filt_order, prepick, length = (
        2, 10, 25, 4, 0.5, 6)
    t1 = UTCDateTime(2015, 5, 4)
    t2 = t1 + 86400
    # We need the catalog
    catalog = Catalog()
    warnings.filterwarnings("ignore", category=UserWarning)
    for sfile in glob.glob(directory + '/data/previous_work/sfiles/*'):
        catalog += read_events(sfile)
    # Need to convert picks to three letter names
    for event in catalog:
        for pick in event.picks:
            pick.waveform_id.channel_code = (
                pick.waveform_id.channel_code[0] + 'H' +
                pick.waveform_id.channel_code[-1])
        # There are multiple picks sometimes, we need to remove them.
        # ASPR is not great - gappy and breaks things, don't use.
        picked = []
        _picks = []
        for pick in event.picks:
            if pick.waveform_id.station_code == 'ASPR':
                continue
            if pick.waveform_id.station_code in ['STBA', 'JCZ', 'MSZ']:
                # Don't use these stations as they fit poorly with the
                # velocity model
                continue
            new_pick = [pick.waveform_id, pick.phase_hint]
            if new_pick not in picked:
                picked.append(new_pick)
                _picks.append(pick)
        event.picks = _picks
    # Add in approximate moment-tensor information from
    # Warren-Smith et al., 2017's focal mechanism for the Wanaka mainshock.
    fm = FocalMechanism(nodal_planes=NodalPlanes(
        nodal_plane_1=NodalPlane(strike=252.0, dip=58.0, rake=170.0)),
        moment_tensor=MomentTensor(scalar_moment=1e20))
    for event in catalog:
        _fm = deepcopy(fm)
        _fm.triggering_origin_id = event.origins[0].resource_id
        event.focal_mechanisms.append(_fm)
    catalog.write("real_template_picks.xml", format="QUAKEML")
    data = read(directory + '/data/continuous_data/*')
    data.merge()
    data.trim(t1, t2)
    for tr in data:
        if tr.stats.npts > tr.stats.sampling_rate * (t2 - t1):
            tr.data = tr.data[0:int(tr.stats.sampling_rate * (t2 - t1))]
    data.split()
    # Get the station information
    client = Client("GEONET")
    bulk = []
    for tr in data.select(network="NZ"):
        bulk.append((tr.stats.network, tr.stats.station, tr.stats.location,
                     tr.stats.channel, tr.stats.starttime, tr.stats.endtime))
    inv = client.get_stations_bulk(bulk=bulk, level="response")
    # Get station information for COSA data - we haven't got this up to
    # IRIS yet
    cosa_inv = read_inventory(directory + '/data/COSA.seed')
    cosa_inv[0].code = "CO"
    for station in cosa_inv[0]:
        for channel in station:
            if station.code == "HAAS":
                channel.code = "SH{0}".format(channel.code[-1])
            channel.code = "{0}H{1}".format(channel.code[0], channel.code[-1])
            channel.location_code = "10"
    inv += cosa_inv
    # Velocities are: (depth (m), Vp (m/s), Vs (m/s), rho, Qp, Qs)
    # Construct the velocities - Densities and quality factors are from table
    # 1 of Ristau 2008, "Implementation of Routine Regional Moment Tensor
    # Analysis in New Zealand". Velocities are from Warren-Smith et al., 2017
    # Microseismicity in Southern South Island, New Zealand: Implications for
    # the Mechanism of Crustal Deformation Adjacent to a Major Continental
    # Transform
    velocities = [
        (5000., 5500., 3200.,  2670., 400., 200.),
        (38000., 6000., 3500.,  2750., 400., 200.),
        (48000., 6800., 4000.,  2750., 400., 200.),
        (88000., 8100., 4800.,  3040., 400., 200.)]
    # Generate the tribes
    tribes = {'synthetic': generate_synth_templates(
        lowcut, highcut, samp_rate, filt_order, prepick, length,
        catalog=catalog),
              'real': generate_real_templates(
        lowcut, highcut, samp_rate, filt_order, prepick, length, data.copy(),
        catalog_file=catalog),
              'gf': generate_synth_axitra_gf(
        lowcut, highcut, samp_rate, filt_order, prepick, length,
        catalog=catalog, inventory=inv, velocities=velocities, parallel=True)}
    # Remove templates with fewer than five stations
    for tribe in tribes.values():
        enough = []
        for template in tribe:
            template_stations = [tr.stats.station for tr in template.st]
            if len(set(template_stations)) >= 5:
                enough.append(template)
        tribe.templates = enough
    if template_plot:
        # There are no magnitudes for Wanaka events, add some in so it plots
        for event in catalog:
            event.magnitudes.append(Magnitude(mag=3, magnitude_type='ML'))
        catalog.plot(projection='local', resolution='h')
        for key in tribes.keys():
            for template in tribes[key]:
                template.st.plot(size=(800, 600), equal_scale=False)

    # for key in parties.keys():
    #     snr_threshold(parties[key], 3.5, filtered_data)
    # Plot just the detections for the first two hours
    plot_templates = {'synthetic': tribes['synthetic'][0]}
    _ot = plot_templates['synthetic'].event.origins[0].time
    fm = plot_templates['synthetic'].event.focal_mechanisms[0]
    for key in ['gf', 'real']:
        for template in tribes[key]:
            if template.event.origins[0].time == _ot:
                plot_templates.update({key: template})
    parties = detect_and_plot(
        tribes=tribes, directory=directory, catalog=catalog, data=data,
        inv=inv, verbose=verbose, lowcut=lowcut, highcut=highcut,
        snr_thresh=3.0, iteration=1, plot_templates=plot_templates)
    second_round_tribes = {
        key: party_to_tribe(party=parties[key], data=data.copy())
        for key in parties.keys()}
    for tribe in second_round_tribes.values():
        for template in tribe:
            for tr in template.st:
                if tr.stats.npts != tr.stats.sampling_rate * length:
                    print(tr)
                    template.st.remove(tr)
                if tr.data.var() < 0.01:
                    template.st.remove(tr)
            if len(template.st) <= 5:
                tribe.remove(template)
    second_round_parties = detect_and_plot(
        tribes=second_round_tribes, directory=directory, catalog=catalog,
        data=data, inv=inv, verbose=verbose, lowcut=lowcut, highcut=highcut,
        snr_thresh=0.0, iteration=2, plot_templates=plot_templates,
        template_catalog=catalog)

    # Get sta/lta detections
    energy_detections = cosa_network_trigger(data.select(component='Z'))
    fig = plot_detection_rate(
        parties=second_round_parties, data=data, station="HUVA", channel="HHZ",
        sta_lta=[t['time'] for t in energy_detections])


if __name__ == '__main__':
    run()

#!/usr/bin/env python
"""
    Examples of synthetic template matched-filter earthquake detection.

    :author:    Calum J. Chamberlain
    :data:      18/10/2017

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import glob
import os

from utils.utilities import _log_print


def _collect_examples(example_paths):
    """Get all the examples."""
    examples = []
    for example in example_paths:
        try:
            example_mod_name = '.'.join(os.path.split(example))[0:-3]
            example_mod = __import__(example_mod_name)
            examples.append(example_mod)
        except ImportError as e:
            print("Cannot import from %s" % example)
            print(e)
    return examples


def run(examples, template_plot=False, verbose=False, anti=False):
    """Run all examples."""
    for example in examples:
        if anti:
            example.run_anti_example.run(template_plot=template_plot, verbose=verbose)
        else:
            example.run_example.run(template_plot=template_plot, verbose=verbose)
        _log_print("Finished example", "x")


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Run examples of matched-filter earthquake detection '
                    'using synthetic templates')
    parser.add_argument('-n', '--name', help='Specific test to run',
                        required=False)
    parser.add_argument('-p', '--template-plot', help='Plot templates',
                        required=False, action='store_true')
    parser.add_argument('-v', '--verbose', help='Moar output', required=False,
                        action='store_true')
    parser.add_argument('-a', '--anti', help='Run the null case - only for LFE',
                        required=False, action='store_true')
    args = vars(parser.parse_args())
    example_paths = glob.glob('*/run_example.py')
    anti_path = glob.glob('LFE/run_anti_example.py')
    if args['template_plot']:
        template_plot = True
    else:
        template_plot = False
    if args['verbose']:
        verbose = True
    else:
        verbose = False
    if args['anti']:
        anti = True
        example_paths = anti_path
    else:
        anti = False
    if args['name']:
        try:
            example_paths = [example_path for example_path in example_paths
                             if args['name'] in example_path.split(os.sep)]
        except:
            raise IOError("Unknown name: %s" % args['name'])
        if anti:
            if args['name'] != 'LFE':
                raise IOError("Null case only available for LFE")
    examples = _collect_examples(example_paths=example_paths)
    _log_print("Collected %i examples" % len(examples), "x")
    run(examples=examples, template_plot=template_plot, verbose=verbose, anti=anti)

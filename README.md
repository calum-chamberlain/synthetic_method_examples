Examples for synthetic template matched-filters
=============================

The directories herein contain the data and functions used for the examples in Chamberlain et al. (2018). All examples can be re-run by calling, either each `run_example.py` file in each directory, or by calling the main `run_all_examples.py` file. All code within this project is licensed under the GNU General Public Licence.  You should have received a licence with this repository, but if not, see [GNU GPL](https://www.gnu.org/licenses).

This README was last modified on Wednesday, 26. April 2018 22:49 (UTC)

Requirements:
-------------
To be able to run the examples you need to have installed the dependancies. The simplest way
to do this (and to not damage your python system) is to using anaconda and a virtual
environment.  If you do not already have an anaconda or miniconda install go to
the [conda-install](https://conda.io/docs/user-guide/install/index.html#installing-conda-on-a-system-that-has-other-python-installations-or-packages)
instructions and get yourself set-up.

Once you have a conda install, create a new environment with (nearly) all the dependencies using:
```bash
conda config --add channels conda-forge
conda create -n synthetics python=3.6 pip obspy=1.1.1 eqcorrscan matplotlib numpy pandas xarray
source activate synthetics
conda install gmt -c conda-forge/label/dev
pip install https://github.com/GenericMappingTools/gmt-python/archive/master.zip
pip install mplstereonet
```

You will also have to install axitra which we use for generating green's functions
and synthetic seismograms, available to download [here](https://isterre.fr/staff-directory/member-web-pages/olivier-coutant/article/logiciels-softwares?lang=fr)
You will have to read their readme, and replace the convm.f souirce-file with the
version supplied here, which allows the program to accept command line arguments 
(the functionality of the code is unaffected).

You will need to set the AXITRA_PATH variable in the `utils/utilities.py` file to where your
axitra binary is located.

---

# Running the examples:

Just run:
```bash
python run_all_examples.py
```

This script supports a few flags (`-n` allows you to specify a particular example to run,
other flags will be shown by running `python run_all_examples.py -h`), which you may wish
to make use of.

This will output some progress and plot the results at the end of each example.

---

# Description of examples:

## Swarm

This example analyses data for the day: 24/05/2009 from a background swarm
analysed by [Boese et al. (2014)](http://onlinelibrary.wiley.com/doi/10.1002/2013GC005171/full).
Included in the *data* directory is the appropriate continuous data from SAMBA
(network documented by [Boese et al. (2012)](http://onlinelibrary.wiley.com/doi/10.1029/2011JB008460/full)
and available from [IRIS](http://ds.iris.edu/mda/9F?timewindow=2008-2020)).
The example will also download the appropriate [GeoNet](https://www.geonet.org.nz/) data.
Within the *data/previous_work* folder is the catalog obtained by
[Boese et al. (2014)](http://onlinelibrary.wiley.com/doi/10.1002/2013GC005171/full).
A local velocity model, used for calculation of moveouts for the
generation of a gird of synthetic templates, is included in the
*data/v_model* directory. Other directories are populated by the example code.

The code will:

1. Generate a grid of synthetic templates;
2. Generate real templates for all earthquakes detected by [Boese et al. (2014)](http://onlinelibrary.wiley.com/doi/10.1002/2013GC005171/full);
3. Use both sets of templates to detect within the continuous data at 8xMAD, with a minimum trigger interval of 6 seconds using the matched-filter routines in EQcorrscan.
4. Compare detections from both template sets to those obtained by [Boese et al. (2014)](http://onlinelibrary.wiley.com/doi/10.1002/2013GC005171/full);
5. Plot the results.

---
## Wanaka

This example analyses data for the day: 04/05/2015 from the aftershock sequence of the
Wanaka M6.0 earthquake analysed by [Warren-Smith et al. (2017)](http://srl.geoscienceworld.org/content/early/2017/06/02/0220170016.abstract).
Included in the *data* directory is the appropriate continuous data from COSA
(network documented by [Warren-Smith et al. (2017b)](http://onlinelibrary.wiley.com/doi/10.1002/2017JB014732/abstract)
Within the *data/previous_work* folder is the catalog obtained by
.

The code will:

1. Generate a grid of synthetic templates;
2. Generate real templates for all earthquakes detected by [Warren-Smith et al. (2017)](http://srl.geoscienceworld.org/content/early/2017/06/02/0220170016.abstract);
3. Use both sets of templates to detect within the continuous data at
   8xMAD, with a minimum trigger interval of 6 seconds using the
   matched-filter routines in EQcorrscan.
4. Compare detections from both template sets to those obtained by [Warren-Smith et al. (2017)](http://srl.geoscienceworld.org/content/early/2017/06/02/0220170016.abstract);
5. Plot the results.
___
## LFE

The example uses 14 LFE templates (Chemberlain et al., 2014)from a burst of
Southern Alps tremor (Wech et al., 2012). The main purpose of this example
is to demonstrate that synthetic and real templates are able to detect tremor
in this region of low-amplitude, short duration tremor.

___
## Sensitiviy

This example runs sensitivity tests for the one earthquake in the GeoNet catalogue.  This earthquake was chosen because
it has limited clustered seismicity, meaning few surrounding earthquakes should be falsely detected.  The sensitivity
tests comprise three separate tests which will test the sensitivity of detections to variations in synthetic template:

1. Focal Mechanism;
2. Moment;
3. Location.

Both simple synthetics generated using spikes, and axitra generated templates are tested.

---
## Adding more examples

To add more examples and for them to be found by `run_all_examples.py`, you should create a new directory for the example, and include an `__init__.py` and `run_example.py` file.  The `__init__.py` file can be blank. The `run_example.py` file must contain a `run()` function which calls everything for the example.  To allow the example to be run from the command line by calling:
```bash
python <example_dir>/run_example.py
```
you must include the following at the bottom of your `run_example.py` file:
```python
if __name__ == '__main__':
    run()
```
where `run()` is the function that runs the example!

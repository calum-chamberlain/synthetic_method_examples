"""
    Example of synthetic template matched-filter detection for earthquake
    swarms.

    :author:    Calum J. Chamberlain
    :data:      18/10/2017

    Licence:
    --------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import os
import glob
import warnings

from multiprocessing import Pool as ProcessPool
from obspy import read, read_events, Catalog, Stream, Trace, UTCDateTime
from obspy.core.event import (
    FocalMechanism, NodalPlane, NodalPlanes, MomentTensor)
from obspy.clients.fdsn import Client
from eqcorrscan.core.match_filter import Tribe, Template, Party
from eqcorrscan.utils.correlate import pool_boy
from eqcorrscan.utils.trigger import network_trigger, TriggerParameters
from copy import deepcopy

from utils.utilities import (
    detect_in_data, snr_threshold, _log_print, seis_sim, axitra_sim,
    party_to_tribe)
from utils.plotting import gmt_triple_plot, plot_detection_rate

from utils.utilities import AXITRA_PATH

# Channel mapping
NZ_channels = {
    'SZ': 'HHZ', 'SN': 'HHN', 'SE': 'HHE', 'S1': 'HH1', 'S2': 'HH2',
    'HZ': 'HHZ', 'HN': 'HHN', 'HE': 'HHE', 'H1': 'HH1', 'H2': 'HH2',
    'HHN': 'HHN', 'HHZ': 'HHZ', 'HHE': 'HHE', 'HH1': 'HH1', 'HH2': 'HH2'}
SAMBA_channels = {
    'SZ': 'EHZ', 'SN': 'EH1', 'SE': 'EH2', 'S1': 'EH1', 'S2': 'EH2',
    'SHZ': 'EHZ', 'SHN': 'EH1', 'SHE': 'EH2', "SH1": "EHZ", "SH2": "EH1",
    "SH3": "EH2", "EHZ": "EHZ", "EHN": "EHN", "EHE": "EHE", "EH1": "EH1",
    "EH2": "EH2"}


def generate_synth_axitra_gf(lowcut, highcut, samp_rate, filt_order, prepick,
                             length, catalog, inventory, velocities,
                             verbose=False, parallel=False, process_len=3600):
    """Generate the synthetic templates from axitra data."""
    tribe = Tribe()
    if not parallel:
        for i, event in enumerate(catalog):
            template = _generate_synth_axitra_gf_single(
                lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
                filt_order=filt_order, prepick=prepick, length=length,
                event=event, inventory=inventory, velocities=velocities,
                verbose=verbose, i=i)
            tribe += template
    else:
        with pool_boy(ProcessPool, len(catalog)) as pool:
            try:
                params = ((
                    lowcut, highcut, samp_rate, filt_order, prepick, length,
                    event, inventory, velocities, verbose, i)
                    for i, event in enumerate(catalog))
                results = [
                    pool.apply_async(_generate_synth_axitra_gf_single, param)
                    for param in params]
                tribe.templates = [res.get() for res in results]
            except Exception as e:
                raise e
    for template in tribe:
        template.process_length = process_len
    if len(tribe) == 0:
        raise IndexError("No templates created, Wah.")
    return tribe


def _generate_synth_axitra_gf_single(lowcut, highcut, samp_rate, filt_order,
                                     prepick, length, event, inventory,
                                     velocities, verbose=False, i=0):
    """
    Generate a single axitra derived synthetic template.
    """
    if len(event.focal_mechanisms) == 0:
        if verbose:
            print("Cannot generate a synthetic, we have no moment tensor!")
            print(event)
        return Template()
    syn_st = axitra_sim(
        event=event, inventory=inventory, velocities=velocities,
        path=AXITRA_PATH, verbose=verbose,
        wavelet_period=0.1, index=i, offset=-10)
    single_tribe = Tribe().construct(
        method='from_meta_file', lowcut=lowcut, highcut=highcut,
        samp_rate=samp_rate, filt_order=filt_order, prepick=prepick,
        st=syn_st, swin='all', meta_file=Catalog([event]), length=length,
        debug=0, all_horiz=True)
    single_tribe[0].name = '_'.join(event.comments[0].text.split())
    return single_tribe[0]


def generate_synth_templates(lowcut, highcut, samp_rate, filt_order, prepick,
                             length, catalog):
    """Generate synthetic templates."""
    tribe = Tribe()
    for event in catalog:
        template = Template(
            name=str(event.origins[0].time.strftime('%Y_%m_%dt%H_%M_%S')),
            st=None, lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
            filt_order=filt_order, process_length=86400, prepick=prepick,
            event=event)
        picks = {'P': [], 'S': []}
        for pick in event.picks:
            if pick.phase_hint in ['P', 'S']:
                picks[pick.phase_hint].append((
                    pick.waveform_id.station_code, pick.time,
                    pick.waveform_id.channel_code))
        stream = Stream()
        for station, p_time, channel in picks['P']:
            try:
                s_pick = [p for p in picks['S'] if p[0] == station][0]
                s_time = s_pick[1]
            except IndexError:
                continue
            sp_time = s_time - p_time
            Z_data = seis_sim(
                sp=int(sp_time * samp_rate), flength=int(length * samp_rate))
            H_data = seis_sim(
                sp=int(sp_time * samp_rate), flength=int(length * samp_rate),
                amp_ratio=3)
            for data, chan in zip([Z_data, H_data], [channel, s_pick[2]]):
                trace = Trace(data=data)
                trace.stats.station = station
                trace.stats.channel = chan
                if len(station) == 4:
                    trace.stats.network = '9F'
                else:
                    trace.stats.network = 'NZ'
                    trace.stats.location = '10'
                trace.stats.sampling_rate = samp_rate
                trace.stats.starttime = p_time - (10 / samp_rate)
                stream += trace
        stream.filter('bandpass', freqmin=lowcut, freqmax=highcut,
                      corners=filt_order)
        template.st = stream
        tribe += template
    return tribe


def generate_real_templates(lowcut, highcut, samp_rate, filt_order, prepick,
                            length, data, catalog):
    """Generate the real templates."""
    tribe = Tribe().construct(
        method='from_meta_file', lowcut=lowcut, highcut=highcut,
        samp_rate=samp_rate, filt_order=filt_order, prepick=prepick, st=data,
        swin='all', meta_file=catalog, length=length, debug=0)
    return tribe


def detect_and_plot(tribes, directory, catalog, data, inv, verbose, lowcut,
                    highcut, plot_templates, snr_thresh=0, iteration=1,
                    template_catalog=None):
    parties = {}
    for key in tribes.keys():
        _log_print("Detecting for %s tribe" % key, ".")
        tribes[key].write(directory + '/templates/' + key + 
                          "_iter_{0}".format(iteration))
        party = detect_in_data(tribes[key], data=data.merge(), verbose=verbose)
        party.write(filename=os.path.join(
            directory, 'data', 'detections', 
            key + '_tribe_iter_{0}'.format(iteration)))
        parties.update({key: party})
    filtered_data = data.split().merge(fill_value='interpolate').copy().filter(
        'bandpass', freqmin=lowcut, freqmax=highcut)
    for key in parties.keys():
        snr_threshold(parties[key], snr_thresh, filtered_data)
    _log_print("Plotting", ".")
    if not template_catalog:
        template_catalog = catalog
    fig = gmt_triple_plot(
        parties=parties, catalog=catalog, station="WHYM", channel="EHZ",
        data=data, plot_len=2.5, inventory=inv, gap=2, catalog_diff=1.5,
        template_catalog=template_catalog,
        templates=plot_templates, label_size=10, label_offset=1.0,
        region_pad=0.1, fm_pad=0.35, scale_length=40, tick_spacing=0.5,
        overview_region=[165.8, 175, -47.2, -40],
        map_region=[169.5, 171, -44.0, -42.8], 
        zoom_region=[170.35, 170.4, -43.4925, -43.455],
        zoom_scale_length=1, template_offset=4.1,
        template_height=3.8, fm=[fm.nodal_planes.nodal_plane_1.strike,
            fm.nodal_planes.nodal_plane_1.dip,
            fm.nodal_planes.nodal_plane_1.rake])
    fig.savefig("{0}{1}plots{1}{2}_gmt_triple_plot_iteration_{3}.eps".format(
        directory, os.path.sep, os.path.split(directory)[-1], iteration))
    return parties


def swarm_network_trigger(st):
    """
    Use coincidence trigger with sta/lta to detect.
    """
    st = st.split().merge(method=1, fill_value="interpolate")
    parameters = []
    for tr in st:
        parameters.append(TriggerParameters(
            {'station': tr.stats.station, 'channel': tr.stats.channel,
            'sta_len': 0.2, 'lta_len': 10.0, 'thr_on': 7.0,
            'thr_off': 5.0, 'lowcut': 2.0, 'highcut': 15.0}))
    triggers = network_trigger(
        st=st, parameters=parameters, thr_coincidence_sum=5, moveout=30,
        max_trigger_length=20, despike=False)
    return triggers


def run(template_plot=False, verbose=False):
    directory = os.path.abspath(os.path.dirname(__file__))
    _log_print(("Running " + os.path.split(directory)[-1] +
                " example").center(80), "=")
    lowcut, highcut, samp_rate, filt_order, prepick, length = (
        2, 10, 25, 4, 0.5, 6)
    # We need the catalog
    catalog = Catalog()
    warnings.filterwarnings("ignore", category=UserWarning)
    for sfile in glob.glob(directory + '/data/previous_work/sfiles/*'):
        catalog += read_events(sfile)
    t1 = UTCDateTime(
        sorted(catalog, key=lambda e:
               e.origins[0].time)[0].origins[0].time.date)
    t2 = t1 + 86400
    # Need to convert picks to three letter names
    bulk = []
    for event in catalog:
        phase_picks = []
        for pick in event.picks:
            if pick.phase_hint not in ["P", "S"]:
                continue
            pick.waveform_id.station_code = \
                pick.waveform_id.station_code.upper()
            if pick.waveform_id.station_code in ["POCR", "WHAT"]:
                pick.waveform_id.station_code += "2"
                pick.waveform_id.location_code = '10'
            if len(pick.waveform_id.station_code) == 3:
                pick.waveform_id.network_code = 'NZ'
                pick.waveform_id.location_code = '10'
                pick.waveform_id.channel_code = \
                    NZ_channels[pick.waveform_id.channel_code]
                pick_info = (
                    pick.waveform_id.network_code,
                    pick.waveform_id.station_code,
                    pick.waveform_id.location_code,
                    pick.waveform_id.channel_code[0:2] + "*", t1, t2)
                if pick_info not in bulk:
                    bulk.append(pick_info)
            else:
                pick.waveform_id.network_code = '9F'
                pick.waveform_id.channel_code = \
                    SAMBA_channels[pick.waveform_id.channel_code]
            phase_picks.append(pick)
        event.picks = phase_picks
    # Add in approximate moment-tensor information from Boese et al., 2014's
    # focal mechanism for 2014/05/29.
    fm = FocalMechanism(nodal_planes=NodalPlanes(
        nodal_plane_1=NodalPlane(strike=281.5, dip=83.5, rake=145.4)),
        moment_tensor=MomentTensor(scalar_moment=1e20))
    for event in catalog:
        _fm = deepcopy(fm)
        _fm.triggering_origin_id = event.origins[0].resource_id
        event.focal_mechanisms.append(_fm)
    data = read(directory + '/data/continuous_data/*')
    client = Client("GEONET")
    data += client.get_waveforms_bulk(bulk=bulk)
    data.merge()
    for tr in data:
        tr.stats.station = tr.stats.station.upper()
        if tr.stats.station in ["POCR", "WHAT"]:
            tr.stats.station += "2"
            tr.stats.location = '10'
        if len(tr.stats.station) == 3:
            tr.stats.network = 'NZ'
            tr.stats.location = '10'
            tr.stats.channel = NZ_channels[tr.stats.channel]
        else:
            tr.stats.network = '9F'
            tr.stats.channel = SAMBA_channels[tr.stats.channel]
    # Get the station information
    bulk = []
    for tr in data.select(network="NZ"):
        bulk.append((tr.stats.network, tr.stats.station, tr.stats.location,
                     tr.stats.channel, tr.stats.starttime, tr.stats.endtime))
    inv = client.get_stations_bulk(bulk=bulk, level="response")
    client = Client("IRIS")
    inv += client.get_stations(network="9F", starttime=data[0].stats.starttime,
                               endtime=data[0].stats.endtime, level="response")
    _inv = deepcopy(inv)
    _inv.networks = []
    for network in inv:
        _net = network.copy()
        _net.stations = []
        for station in network:
            if station.code in [tr.stats.station for tr in data]:
                _net.stations.append(station)
        if len(_net.stations) > 0:
            _inv.networks.append(_net)
    inv = _inv
    # The last three hours of data on MTFO are badly affected by spikes
    data.select(station="MTFO").trim(t1, t2 - (3 * 3600))
    data.trim(t1, t2)
    for tr in data:
        if len(tr.data) > int((t2 - t1) * tr.stats.sampling_rate):
            tr.data = tr.data[0:int((t2 - t1) * tr.stats.sampling_rate)]
    data = data.split()
    # Velocities are: (depth (m), Vp (m/s), Vs (m/s), rho, Qp, Qs)
    # Construct the velocities - These are taken from table 1 of Ristau 2008,
    # "Implementation of Routine Regional Moment Tensor Analysis in New
    # Zealand". Velocities used here are those for "South Island" in table 1
    # velocities = [
    #     (3000., 5000., 3000.,  2670., 400., 200.),
    #     (12000., 6000., 3600.,  2700., 400., 200.),
    #     (27000., 6500., 3700.,  2720., 400., 200.),
    #     (39000., 7500., 4300.,  2750., 400., 200.),
    #     (58000., 8100., 4600.,  3040., 400., 200.)]
    # I don't think this is good enough - phases don't align.
    # Carolin's velocities - using vp/vs ratio of 1.68
    velocities = [
        (8000., 5670., 3375.,  2670., 400., 200.),
        (18000., 5790., 3446.4,  2700., 400., 200.),
        (35000., 6280., 3738.,  2720., 400., 200.),
        (40000., 7350., 4375.,  2750., 400., 200.),
        (80000., 8000., 4761.,  3040., 400., 200.)]
    # Generate the tribes
    tribes = {'synthetic': generate_synth_templates(
        lowcut, highcut, samp_rate, filt_order, prepick, length,
        catalog=catalog),
              'real': generate_real_templates(
        lowcut, highcut, samp_rate, filt_order, prepick, length,
        data=data.copy(), catalog=catalog),
              'gf': generate_synth_axitra_gf(
        lowcut, highcut, samp_rate, filt_order, prepick, length,
        catalog=catalog, inventory=inv, velocities=velocities,
        parallel=True, process_len=86400)}
    # Remove templates with fewer than five stations
    for tribe in tribes.values():
        enough = []
        for template in tribe:
            template_stations = [tr.stats.station for tr in template.st]
            if len(set(template_stations)) >= 5:
                enough.append(template)
        tribe.templates = enough
    if template_plot:
        catalog.plot(projection='local', resolution='h')
        for key in tribes.keys():
            for template in tribes[key]:
                template.st.plot(size=(800, 600), equal_scale=False)
    plot_templates = {'synthetic': tribes['synthetic'][0]}
    _ot = plot_templates['synthetic'].event.origins[0].time
    fm = plot_templates['synthetic'].event.focal_mechanisms[0]
    for key in ['gf', 'real']:
        for template in tribes[key]:
            if template.event.origins[0].time == _ot:
                plot_templates.update({key: template})
    parties = detect_and_plot(
        tribes=tribes, directory=directory, catalog=catalog, data=data,
        inv=inv, verbose=verbose, lowcut=lowcut, highcut=highcut,
        snr_thresh=3.0, iteration=1, plot_templates=plot_templates)
    second_round_tribes = {
        key: party_to_tribe(party=parties[key], data=data.copy())
        for key in parties.keys()}
    second_round_parties = detect_and_plot(
        tribes=second_round_tribes, directory=directory, catalog=catalog,
        data=data, inv=inv, verbose=verbose, lowcut=lowcut, highcut=highcut,
        snr_thresh=0.0, iteration=2, plot_templates=plot_templates,
        template_catalog=catalog)

    triggers = swarm_network_trigger(data.select(component="Z").copy())
    fig = plot_detection_rate(
        parties=second_round_parties, data=data, station="WHYM", channel="EHZ",
        sta_lta=[t['time'] for t in triggers])


if __name__ == '__main__':
    run()

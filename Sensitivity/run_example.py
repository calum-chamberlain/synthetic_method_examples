"""
    Sensitivity tests for synthetic templates.

    :author:    Calum J. Chamberlain
    :data:      18/10/2017

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import os
import warnings

from utils.utilities import (
    _log_print, axitra_sim, seis_sim, detect_in_data, predict_pick_times)
from utils.plotting import (
    multi_beach_plot, location_sensitivity_plot,
    single_beach_multi_mechanism_plot)
from utils.coordinates import Geographic, Location

from copy import deepcopy
from obspy import Stream, Trace, Catalog, read_events, read
from obspy.core.event import (
    FocalMechanism, NodalPlane, NodalPlanes, MomentTensor, Comment)
from obspy.clients.fdsn import Client
from eqcorrscan import Tribe, Template, Party
from eqcorrscan.utils.correlate import pool_boy
from multiprocessing import Pool as ProcessPool

from utils.utilities import AXITRA_PATH

NZ_channels = {
    'SZ': 'HHZ', 'SN': 'HHN', 'SE': 'HHE', 'S1': 'HH1', 'S2': 'HH2',
    'HZ': 'HHZ', 'HN': 'HHN', 'HE': 'HHE', 'H1': 'HH1', 'H2': 'HH2',
    'HHN': 'HHN', 'HHZ': 'HHZ', 'HHE': 'HHE', 'HH1': 'HH1', 'HH2': 'HH2'}
SAMBA_channels = {
    'SZ': 'EHZ', 'SN': 'EH1', 'SE': 'EH2', 'S1': 'EH1', 'S2': 'EH2',
    'SHZ': 'EHZ', 'SHN': 'EH1', 'SHE': 'EH2', "SH1": "EHZ", "SH2": "EH1",
    "SH3": "EH2", "EHZ": "EHZ", "EHN": "EHN", "EHE": "EHE", "EH1": "EH1",
    "EH2": "EH2"}
MAX_THREADS = 20

directory = os.path.abspath(os.path.dirname(__file__))


def focal_mechanism_sensitivity(data, event, strike_range, dip_range,
                                rake_range, lowcut, highcut, samp_rate,
                                filt_order, prepick, length, inventory,
                                velocities, verbose=False):
    """
    Test the sensitivity of detections to changes in strike, dip and rake of
    focal mechanism.

    :type data: obspy.core.stream.Stream
    :param data: Data to detect within.
    :type event: obspy.core.event.Event
    :param event: Event known to be in the data to test against
    :type strike_range: numpy.array
    :param strike_range: Range of strikes to be tested
    :type dip_range: numpy.array
    :param dip_range: Range of dips to be tested
    :type rake_range: numpy.array
    :param rake_range: Range of rakes to be tested

    :return: eqcorrscan.core.match_filter.Party of detections
    """
    _log_print(
        "Running sensitivity test for variations in focal mechanism", "-")
    altered_catalog = Catalog()
    for strike_var in strike_range:
        for dip_var in dip_range:
            for rake_var in rake_range:
                altered_ev = event.copy()
                altered_ev.focal_mechanisms[
                    0].nodal_planes.nodal_plane_1.strike += strike_var
                altered_ev.focal_mechanisms[
                    0].nodal_planes.nodal_plane_1.dip += dip_var
                altered_ev.focal_mechanisms[
                    0].nodal_planes.nodal_plane_1.rake += rake_var
                altered_ev.comments = [Comment(
                    text="rotated by {0}deg strike {1}deg rake "
                         "{2}deg dip".format(strike_var, rake_var, dip_var))]
                altered_catalog.append(altered_ev)
    tribes = {
        'gf': generate_synth_axitra_gf(
            lowcut, highcut, samp_rate, filt_order, prepick, length,
            catalog=altered_catalog, inventory=inventory, velocities=velocities,
            parallel=True)}
    # Note that the simple synthetics have no dependence on mechanisms
    parties = {}
    for key in tribes.keys():
        _log_print("Detecting for %s tribe" % key, ".")
        tribes[key].write(directory + '/templates/' + key +
                          "_focal_mechanism_sensitivity")
        party = detect_in_data(tribes[key], data=data, verbose=verbose,
                               daylong=False, decluster=False, cores=10)
        parties.update({key: party})
    self_det_parties = {}
    for key, party in parties.items():
        self_det_parties.update({key: _get_detections_close_to_master(
            party=party, master=event, window=2)})
        party_comment_text = [' '.join(f.template.name.split('_'))
                              for f in self_det_parties[key]]
        detections = []
        detect_vals = []
        for ev in altered_catalog:
            if ev.comments[0].text in party_comment_text:
                detections.append(True)
                detect_vals.append(self_det_parties[key][
                    party_comment_text.index(ev.comments[0].text)].detections[0].detect_val)
            else:
                detections.append(False)
                detect_vals.append(np.nan)
        try:
            master = [ev.comments[0].text for ev in altered_catalog].index(
                'rotated by 0deg strike 0deg rake 0deg dip')
        except ValueError:
            master = None
        # Do in 1000 mechanism blocks
        # i = 0
        # step = 1000
        # while i < len(altered_catalog):
        #     start_slice = i
        #     end_slice = i + step
        #     _log_print(
        #         "Plotting mechanisms {0} to {1}".format(
        #             start_slice, end_slice), ".")
        #     fig = multi_beach_plot(
        #         altered_catalog[start_slice:end_slice], 
        #         detections[start_slice: end_slice], master)
        #     fig.savefig(
        #         "{0}/plots/{1}_focal_mechanism_sensitivity_{2}-{3}.eps".format(
        #             directory, key, start_slice, end_slice))
        #     fig.clf()
        #     i += step
        fig = single_beach_multi_mechanism_plot(
            catalog=altered_catalog, detections=detections, detect_vals=detect_vals,
            master=master, p_axis=True)
        fig.savefig(
            "{0}/plots/{1}_focal_mechanism_sensitivity_multi_beach.eps".format(
                directory, key))
        fig.clf()
    return parties


def moment_sensitivity(data, event, moment_range, lowcut, highcut, samp_rate,
                       filt_order, prepick, length, inventory, velocities,
                       verbose=False):
    """
    Test the sensitivity of detections to changes in scalar moment.

    :type data: obspy.core.stream.Stream
    :param data: Data to detect within.
    :type event: obspy.core.event.Event
    :param event: Event known to be in the data to test against
    :type moment_range: numpy.array
    :param moment_range: Range of multipliers of moment to test.

    :return: eqcorrscan.core.match_filter.Party of detections
    """
    _log_print("Running sensitivity test for variations in moment", "-")
    catalog = Catalog()
    for moment_var in moment_range:
        altered_ev = event.copy()
        altered_ev.magnitudes[0].mag += moment_var
        altered_ev.focal_mechanisms[0].moment_tensor.scalar_moment *= \
            10.0 ** moment_var
        altered_ev.comments = [Comment(
            text="magnitude scaled by {0}".format(moment_var))]
        catalog.append(altered_ev)
    tribes = {
        'gf': generate_synth_axitra_gf(
            lowcut, highcut, samp_rate, filt_order, prepick, length,
            catalog=catalog, inventory=inventory, velocities=velocities,
            parallel=True)}
    # Note that the simple synthetics have no dependence on magnitude
    parties = {}
    for key in tribes.keys():
        _log_print("Detecting for %s tribe" % key, ".")
        tribes[key].write(directory + '/templates/' + key)
        party = detect_in_data(tribes[key], data=data, verbose=verbose,
                               daylong=False, decluster=False)
        party.write(
            filename=os.path.join(
                directory, 'data', 'detections', key + '_tribe'))
        parties.update({key: party})
    return parties


def location_sensitivity(data, master, x_range, y_range, depth_range, lowcut,
                         highcut, samp_rate, filt_order, prepick, length,
                         inventory, velocities, verbose=False,
                         synth_length=50):
    """
    Test the sensitivity of detections to changes in template location.

    :type data: obspy.core.stream.Stream
    :param data: Data to detect within.
    :type event: obspy.core.event.Event
    :param event: Event known to be in the data to test against
    :type x_range: numpy.array
    :param x_range: Range of shifts from origin in longitude (units: km)
    :type y_range: numpy.array
    :param y_range: Range of shifts from origin in latitude (units: km)
    :type depth_range: numpy.array
    :param depth_range: Range of shifts from origin in longitude (units: km)

    :return: eqcorrscan.core.match_filter.Party of detections
    """
    _log_print("Running sensitivity test for variations in location", "-")
    catalog = Catalog()
    master_origin = master.preferred_origin() or master.origins[0]
    master_loc = Geographic(latitude=master_origin.latitude,
                            longitude=master_origin.longitude,
                            depth=master_origin.depth / -1000)
    # Check that no positive depths occur - taup will error
    _depth_range = []
    for depth_var in depth_range:
        if master_loc.depth + depth_var >= 0:
            print("Positive depth found, not using this")
        else:
            _depth_range.append(depth_var)
    depth_range = _depth_range
    x_y_z = ((x_var, y_var, depth_var)
             for depth_var in depth_range for y_var in y_range
             for x_var in x_range)
    n_variations = len(x_range) * len(y_range) * len(depth_range)
    if n_variations > MAX_THREADS:
        threads = MAX_THREADS
    else:
        threads = n_variations
    with pool_boy(ProcessPool, threads) as pool:
        try:
            results = [pool.apply_async(
                _shift_event_location, (master, master_origin, master_loc,
                                        inventory, x_var, y_var, depth_var))
                for x_var, y_var, depth_var in x_y_z]
            catalog.events = [res.get() for res in results]
        except Exception as e:
            raise e
    tribes = {
        'synthetic': generate_synth_templates(
            lowcut, highcut, samp_rate, filt_order, prepick, synth_length,
            phase_length=length, catalog=catalog),
        'gf': generate_synth_axitra_gf(
            lowcut, highcut, samp_rate, filt_order, prepick, length,
            catalog=catalog, inventory=inventory, velocities=velocities,
            parallel=True)}
    parties = {}
    for key in tribes.keys():
        _log_print("Detecting for %s tribe" % key, ".")
        tribes[key].write('{0}/templates/{1}_location_sensitivity'.format(
            directory, key))
        party = detect_in_data(
            tribes[key], data=data, verbose=verbose, daylong=False,
            decluster=False)
        parties.update({key: party})
    self_det_parties = {}
    for key, party in parties.items():
        self_det_parties.update({key: _get_detections_close_to_master(
            party=party, master=master, window=2)})
    for key in tribes.keys():
        _log_print("Plotting for {0} tribe".format(key), '.')
        try:
            fig = location_sensitivity_plot(
                party=self_det_parties[key], tribe=tribes[key],
                inventory=inventory, master=master, latlon=True,
                plot_template_locs=False)
            fig.savefig("{0}/plots/{1}_location_sensitivity.eps".format(
                directory, key))
            fig.clf()
        except (IndexError, AttributeError):
            pass
    return parties


def _get_detections_close_to_master(party, master, window=20):
    """
    Get the detections within n seconds of the master origin time.

    :type party: eqcorrscan.core.match_filter.Party
    :param party: Party to extract from
    :type master: obspy.core.event.Event
    :param master: Event with origin time
    :type window: float
    :param window: Time in seconds (+/-) of the origin time to allow

    :return: New party
    """
    party_out = Party()
    for family in party:
        _f = family.copy()
        _f.detections = []
        for detection in family:
            if 0 < detection.detect_time - master.origins[0].time < window:
                _f.detections.append(detection)
        if len(_f) > 0:
            party_out += _f
    return party_out


def _shift_event_location(master, master_origin, master_loc, inventory,
                          x_var, y_var, depth_var):
    # Convert x and y to long and lat
    altered_loc = Location(x=x_var, y=y_var, z=depth_var,
                           origin=master_loc, dip=90, strike=0)
    altered_loc = altered_loc.to_geographic()
    altered_ev = master.copy()
    altered_ev.origins = [master_origin.copy()]
    altered_ev.origins[0].latitude = altered_loc.latitude
    altered_ev.origins[0].longitude = altered_loc.longitude
    altered_ev.origins[0].depth = altered_loc.depth * -1000
    altered_ev.preferred_origin_id = \
        altered_ev.origins[0].resource_id
    altered_ev = predict_pick_times(altered_ev, inventory)
    x_pol, y_pol, depth_pol = ("pos", "pos", "pos")
    if x_var < 0:
        x_pol = "neg"
    if y_var < 0:
        y_pol = "neg"
    if depth_var < 0:
        depth_pol = "neg"
    altered_ev.comments = [Comment(
        text="moved by x {0}{1}km y {2}{3}km depth "
             "{4}{5}km".format(
                x_pol, '_'.join(str(abs(x_var)).split('.')),
                y_pol, '_'.join(str(abs(y_var)).split('.')),
                depth_pol, '_'.join(str(abs(depth_var)).split('.'))))]
    return altered_ev


def generate_synth_axitra_gf(lowcut, highcut, samp_rate, filt_order, prepick,
                             length, catalog, inventory, velocities,
                             verbose=False, parallel=False, process_len=3600):
    """Generate the synthetic templates from axitra data."""
    tribe = Tribe()
    if not parallel:
        for i, event in enumerate(catalog):
            template = _generate_synth_axitra_gf_single(
                lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
                filt_order=filt_order, prepick=prepick, length=length,
                event=event, inventory=inventory, velocities=velocities,
                verbose=verbose, i=i)
            tribe += template
    else:
        if len(catalog) > MAX_THREADS:
            threads = MAX_THREADS
        else:
            threads = len(catalog)
        with pool_boy(ProcessPool, threads) as pool:
            try:
                params = ((
                    lowcut, highcut, samp_rate, filt_order, prepick, length,
                    event, inventory, velocities, verbose, i)
                    for i, event in enumerate(catalog))
                results = [
                    pool.apply_async(_generate_synth_axitra_gf_single, param)
                    for param in params]
                tribe.templates = [res.get() for res in results]
            except Exception as e:
                raise e
    for template in tribe:
        template.process_length = process_len
    if len(tribe) == 0:
        raise IndexError("No templates created, Wah.")
    return tribe


def _generate_synth_axitra_gf_single(lowcut, highcut, samp_rate, filt_order,
                                     prepick, length, event, inventory,
                                     velocities, verbose=False, i=0):
    """
    Generate a single axitra derived synthetic template.
    """
    if len(event.focal_mechanisms) == 0:
        if verbose:
            print("Cannot generate a synthetic, we have no moment tensor!")
            print(event)
        return Template()
    syn_st = axitra_sim(
        event=event, inventory=inventory, velocities=velocities,
        path=AXITRA_PATH, verbose=verbose, offset=-10,
        wavelet_period=0.1, index=i)
    single_tribe = Tribe().construct(
        method='from_meta_file', lowcut=lowcut, highcut=highcut,
        samp_rate=samp_rate, filt_order=filt_order, prepick=prepick,
        st=syn_st, swin='all', meta_file=Catalog([event]), length=length,
        debug=0, all_horiz=True)
    single_tribe[0].name = '_'.join(event.comments[0].text.split())
    return single_tribe[0]


def generate_synth_templates(lowcut, highcut, samp_rate, filt_order, prepick,
                             length, phase_length, catalog, process_len=3600):
    """Generate synthetic templates."""
    tribe = Tribe()
    for event in catalog:
        template = Template(
            name='_'.join(event.comments[0].text.split()),
            st=None, lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
            filt_order=filt_order, process_length=process_len, prepick=prepick,
            event=event)
        picks = {'P': [], 'S': []}
        for pick in event.picks:
            if pick.phase_hint in ['P', 'S']:
                picks[pick.phase_hint].append((
                    pick.waveform_id.station_code, pick.time,
                    pick.waveform_id.channel_code))
        stream = Stream()
        for station, p_time, channel in picks['P']:
            try:
                s_pick = [p for p in picks['S'] if p[0] == station][0]
                s_time = s_pick[1]
            except IndexError:
                continue
            sp_time = s_time - p_time
            z_data = seis_sim(
                sp=int(sp_time * samp_rate), flength=int(length * samp_rate),
                sine_samp=0.05)
            h_data = seis_sim(
                sp=int(sp_time * samp_rate), flength=int(length * samp_rate),
                amp_ratio=3, sine_samp=0.05)
            for data, chan in zip([z_data, h_data], [channel, s_pick[2]]):
                trace = Trace(data=data)
                trace.stats.station = station
                trace.stats.channel = chan
                if len(station) >= 4:
                    trace.stats.network = '9F'
                else:
                    trace.stats.network = 'NZ'
                    trace.stats.location = '10'
                trace.stats.sampling_rate = samp_rate
                trace.stats.starttime = p_time - (10 / samp_rate)
                if chan == channel:
                    trace.trim(starttime=p_time - (10 / samp_rate),
                               endtime=p_time - (10 / samp_rate) + phase_length)
                else:
                    trace.trim(starttime=s_time - (10 / samp_rate),
                               endtime=s_time - (10 / samp_rate) + phase_length)
                stream += trace
        stream.filter('bandpass', freqmin=lowcut, freqmax=highcut,
                      corners=filt_order)
        if len(stream) != 0:
            template.st = stream
            tribe += template
    return tribe


def run(template_plot=False, verbose=False):
    _log_print(("Running " + os.path.split(directory)[-1] +
                " example").center(80), "=")
    lowcut, highcut, samp_rate, filt_order, prepick, length = (
        2, 10, 25, 4, 0.5, 6)
    # We need the catalog
    warnings.filterwarnings("ignore", category=UserWarning)
    sfile = (
        "{0}/../Swarm/data/previous_work/sfiles/"
        "24-0711-22L.S200905".format(directory))
    catalog = read_events(sfile)
    t1 = catalog[0].origins[0].time - 1800
    t2 = t1 + 3600
    # Need to convert picks to three letter names
    bulk = []
    for event in catalog:
        phase_picks = []
        for pick in event.picks:
            if pick.phase_hint not in ["P", "S"]:
                continue
            pick.waveform_id.station_code = \
                pick.waveform_id.station_code.upper()
            if pick.waveform_id.station_code in ["POCR", "WHAT"]:
                pick.waveform_id.station_code += "2"
                pick.waveform_id.location_code = '10'
            if len(pick.waveform_id.station_code) == 3:
                pick.waveform_id.network_code = 'NZ'
                pick.waveform_id.location_code = '10'
                pick.waveform_id.channel_code = \
                    NZ_channels[pick.waveform_id.channel_code]
                pick_info = (
                    pick.waveform_id.network_code,
                    pick.waveform_id.station_code,
                    pick.waveform_id.location_code,
                    pick.waveform_id.channel_code[0:2] + "*", t1, t2)
                if not pick_info in bulk:
                    bulk.append(pick_info)
            else:
                pick.waveform_id.network_code = '9F'
                pick.waveform_id.channel_code = \
                    SAMBA_channels[pick.waveform_id.channel_code]
            phase_picks.append(pick)
        event.picks = phase_picks
    # Add in approximate moment-tensor information from Boese et al., 2014's
    # focal mechanism for 2014/05/29.
    fm = FocalMechanism(nodal_planes=NodalPlanes(
        nodal_plane_1=NodalPlane(strike=281.5, dip=83.5, rake=145.4)),
        moment_tensor=MomentTensor(scalar_moment=1e20))
    for event in catalog:
        _fm = deepcopy(fm)
        _fm.triggering_origin_id = event.origins[0].resource_id
        event.focal_mechanisms.append(_fm)
    data = read(directory + '/../Swarm/data/continuous_data/*')
    client = Client("GEONET")
    data += client.get_waveforms_bulk(bulk=bulk)
    data.merge()
    for tr in data:
        tr.stats.station = tr.stats.station.upper()
        if tr.stats.station in ["POCR", "WHAT"]:
            tr.stats.station += "2"
            tr.stats.location = '10'
        if len(tr.stats.station) == 3:
            tr.stats.network = 'NZ'
            tr.stats.location = '10'
            tr.stats.channel = NZ_channels[tr.stats.channel]
        else:
            tr.stats.network = '9F'
            tr.stats.channel = SAMBA_channels[tr.stats.channel]
    # Get the station information
    bulk = []
    for tr in data.select(network="NZ"):
        bulk.append((tr.stats.network, tr.stats.station, tr.stats.location,
                     tr.stats.channel, tr.stats.starttime, tr.stats.endtime))
    inv = client.get_stations_bulk(bulk=bulk, level="response")
    client = Client("IRIS")
    inv += client.get_stations(network="9F", starttime=data[0].stats.starttime,
                               endtime=data[0].stats.endtime, level="response")
    _inv = deepcopy(inv)
    _inv.networks = []
    for network in inv:
        _net = network.copy()
        _net.stations = []
        for station in network:
            if station.code in [tr.stats.station for tr in data]:
                _net.stations.append(station)
        if len(_net.stations) > 0:
            _inv.networks.append(_net)
    inv = _inv
    data = data.split()
    # Carolin's velocities - using vp/vs ratio of 1.68
    velocities = [
        (8000., 5670., 3375.,  2670., 400., 200.),
        (18000., 5790., 3446.4,  2700., 400., 200.),
        (35000., 6280., 3738.,  2720., 400., 200.),
        (40000., 7350., 4375.,  2750., 400., 200.),
        (80000., 8000., 4761.,  3040., 400., 200.)]
    data.trim(t1, t2)
    event = catalog[0]
    location_parties = location_sensitivity(
       data=data, master=event, x_range=np.arange(-20, 21, 2.5),
       y_range=np.arange(-20, 21, 2.5), depth_range=np.arange(-20, 21, 2.5),
       lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
       filt_order=filt_order, prepick=prepick, length=length, inventory=inv,
       velocities=velocities, verbose=verbose, synth_length=50)
    print("Location variation yielded the following parties:")
    print(location_parties)
    for key, value in location_parties.items():
       value.write(directory + '/detections/location_variation_' + key)

    fm_parties = focal_mechanism_sensitivity(
        data=data, event=event, dip_range=np.arange(-180, 180, 15),
        strike_range=np.arange(-180, 180, 15), rake_range=np.arange(-180, 180, 15),
        lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
        filt_order=filt_order, prepick=prepick, length=length, inventory=inv,
        velocities=velocities, verbose=verbose)
    print("Focal mechanism variation yielded the following parties:")
    print(fm_parties)
    for key, value in fm_parties.items():
        value.write(directory + '/detections/focal_mechanism_variation_' + key)

    #moment_parties = moment_sensitivity(
    #    data=data, event=event, moment_range=np.arange(-3, 5, 1),
    #    lowcut=lowcut, highcut=highcut, samp_rate=samp_rate,
    #    filt_order=filt_order, prepick=prepick, length=length, inventory=inv,
    #    velocities=velocities, verbose=verbose)
    #print("Moment variation yielded the following parties:")
    #print(moment_parties)
    #for key, value in moment_parties.items():
    #    value.write(directory + '/detections/moment_variation_' + key)


if __name__ == '__main__':
    run()

"""
    Utility functions for synthetic examples.

    :author:    Calum J. Chamberlain
    :data:      18/10/2017

    Licence:
    --------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import os
import math
import gmt
from gmt.clib import LibGMT
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from itertools import cycle
from datetime import timedelta, datetime
from obspy.signal.filter import envelope
from obspy.imaging.beachball import beach
from obspy.core.event import NodalPlane

from utils.utilities import _detection_within_catalog
from utils.coordinates import Geographic, Location
from utils.focal_mechanism_utilities import (
    aux_plane, meca_dc2axe, sind, cosd, tand, atand)

COLORS = cycle(['red', 'green', 'blue', 'cyan', 'magenta', 'yellow',
                'black', 'firebrick', 'purple', 'darkgoldenrod', 'gray'])
COLOR_IDS = {'gf': 'magenta', 'real': 'blue', 'synthetic': 'green'}
NAME_MAP = {'gf': "Green's Func.", "real": "Real", "synthetic": "Naive"}
CMAP = plt.cm.get_cmap('RdYlGn')

DIR = os.path.abspath(os.path.dirname(__file__))


def gmt_map(template_catalog, inventory=None, background_catalog=None, fm=None,
            region=None, depth_cpt_range=None, region_pad=0.25,
            tick_spacing=0.25, scale_length=10, fig=None, show=False,
            x_offset=1, y_offset=1.5, fm_pad=0.1,
            overview_region=[165.8, 179.4, -47.2, -34.4],
            zoom_region=None, zoom_scale_length=None):
    """
    Plot a map of the background seismicity and the templates.

    :type template_catalog: `obspy.core.event.Catalog`
    :param template_catalog: Template catalog to plot, coloured by depth
    :type inventory: `obspy.core.inventory.inventory.Inventory`
    :param inventory: Stations to plot.
    :type background_catalog: `obspy.core.event.Catalog`
    :param background_catalog: Background catalog to plot (open circles)
    :type fm: list
    :param fm:
        list of [strike, dip, rake]: focal-mechanism to plot. This will be
        put in bottom right of the map
    :type region: list
    :param region:
        list of [min-longitude, max-longitude, min-latitude, max-latitude]
    :type depth_cpt_range: tuple
    :param depth_cpt_range:
        tuple of (min-depth, max-depth) to normalise depth colour-palette in km
    :type region_pad: float
    :param region_pad:
        Value in degrees to add to region defined by template locations.
    :type tick_spacing: float
    :param tick_spacing: Spacing between labels in degrees.
    :type scale_length: float
    :param scale_length: Length of scale in km
    :type fig: `gmt.Figure`
    :param fig: Figure to plot into
    :type show: bool
    :param show: Whether to show the figure or not
    :type x_offset: float
    :param x_offset: X-offset in inches for plot (from previous origin)
    :type y_offset: float
    :param y_offset: X-offset in inches for plot (from previous origin)
    :type overview_region: list
    :param overview_region: Region to plot for overview map
    :type zoom_region: list
    :param zoom_region: Region for zoom on seismicity
    """
    if not region:
        region = [
            min([e.origins[0].longitude
                 for e in template_catalog]) - region_pad,
            max([e.origins[0].longitude
                 for e in template_catalog]) + region_pad,
            min([e.origins[0].latitude
                 for e in template_catalog]) - region_pad,
            max([e.origins[0].latitude
                 for e in template_catalog]) + region_pad]
    if not depth_cpt_range:
        max_depth = max([e.origins[0].depth for e in template_catalog]) / 1000
        min_depth = 0
    else:
        max_depth = depth_cpt_range[1]
        min_depth = depth_cpt_range[0]
    # Put scale in bottom left
    scale = "f{0}/{1}/{2}/{3}+u".format(
        region[0] + 0.25 * (region[1] - region[0]),
        region[2] + 0.1 * (region[3] - region[2]), region[2], scale_length)
    if not fig:
        fig = gmt.Figure()

    with LibGMT() as lib:
        lib.call_module('gmtset', 'FORMAT_GEO_MAP ddd.xx')
        lib.call_module('gmtset', 'MAP_FRAME_TYPE plain')
        lib.call_module('gmtset', 'FONT_ANNOT_PRIMARY 10p')
        lib.call_module('gmtset', 'FONT_LABEL 10p')
        lib.call_module(
            "makecpt", "-Crainbow -Z -T{0}/{1}/{2} -I > depth.cpt".format(
                min_depth, max_depth, (max_depth - min_depth) / 10))
    fig.basemap(region=region, projection='M3i', frame='a0',
                X="{0}i".format(x_offset), Y="{0}i".format(y_offset))
    # Plot topo - will need to use clib interface - contribute back to
    #  gmt-python?
    _reformat_grid_files(region)
    with LibGMT() as lib:
        lib.call_module(
            'grdview', 'grid_file_tmp.grd -Qc -Ctopo.cpt -Iillumination.grd')
    _clean_grid_files()
    # Plot faults
    fig.plot(data=DIR + "/data/faults_NZ_WGS84.gmt", W=0.85)
    # Plot lakes (global lakes suck for NZ)
    fig.plot(data=DIR + "/data/nz-lakes.gmt", color='white', pen='black')
    fig.coast(frame="a{0}nWSe".format(tick_spacing), resolution="f",
              shorelines="1/black", L=scale)
    # Plot background seismicity
    if background_catalog:
        _mags = []
        for e in background_catalog:
            try:
                mag = e.preferred_magnitude().mag or e.magnitudes[0].mag
            except IndexError:
                mag = 1.0
            _mags.append(mag)
        fig.plot(
            x=np.array([e.origins[0].longitude for e in background_catalog]),
            y=np.array([e.origins[0].latitude for e in background_catalog]),
            style='cc', pen='black', sizes=(np.array(_mags) / 8) ** 2)
    # Plot zoom
    if zoom_region:
        fig.plot(
            x=np.array([zoom_region[0], zoom_region[1], zoom_region[1],
                        zoom_region[0], zoom_region[0]]),
            y=np.array([zoom_region[2], zoom_region[2], zoom_region[3],
                        zoom_region[3], zoom_region[2]]),
            pen="0.9p,red")
    # Plot magnitude scale
    _mags = []
    for e in template_catalog:
        try:
            mag = e.preferred_magnitude().mag or e.magnitudes[0].mag
        except (IndexError, AttributeError):
            mag = 4.0
        _mags.append(mag)
    max_mag, min_mag = (int(max(_mags) + 1), int(min(_mags)))
    mag_x, mag_y = ([region[0] + 0.075], [region[3] - 0.075])
    for mag in range(max_mag - 1, min_mag - 1, -1):
        mag_x.append(mag_x[-1])
        mag_y.append(mag_y[-1] - 0.075)
    fig.plot(x=np.array(mag_x), y=np.array(mag_y), 
             sizes=(np.arange(max_mag, min_mag - 1, -1) / 8) ** 2,
             color="black", pen="black", style="cc")
    # Do some ugly pstext work - output label to temporary file, then plot
    for y_loc, mag in zip(mag_y, range(max_mag, min_mag - 1, -1)):
        with open("text.tmp", "w") as f:
            f.write("{0} {1} {2}".format(
                mag_x[-1] + 0.15, y_loc, "M={0}".format(mag)))
        with LibGMT() as lib:
            lib.call_module(
                "pstext", "text.tmp -F+f{0}p,black,- -Ya{1}i -Xa{2}i".format(
                    10, 0, 0))
        os.remove("text.tmp")
    # Plot stations
    if inventory:
        sta_x, sta_y = ([], [])
        for network in inventory:
            for station in network:
                try:
                    sta_x.append(station.longitude)
                    sta_y.append(station.latitude)
                except AttributeError:
                    print("Station {0} does not have a location".format(
                        station.code))
                    pass
        fig.plot(x=np.array(sta_x), y=np.array(sta_y), style='i0.45',
                 color="red", pen="black")
    # Plot templates
    fig.plot(x=np.array([e.origins[0].longitude for e in template_catalog]),
             y=np.array([e.origins[0].latitude for e in template_catalog]),
             style='cc', C='depth.cpt', pen='black',
             sizes=(np.array(_mags) / 8) ** 2,
             color=np.array([(e.origins[0].depth - min_depth) / 1000
                             for e in template_catalog]))
    if fm:
        with open("focmech.tmp", "w") as f:
            f.write('{0} {1} {2} {3} {4} {5} 2.0 {0} {1}'.format(
                region[0] + fm_pad, region[3] - fm_pad, 0.0, fm[0], fm[1], fm[2]))
        with LibGMT() as lib:
            lib.call_module('psmeca', 'focmech.tmp -Sa2.5')
        os.remove('focmech.tmp')
    # Plot overview - temporary shift in origin prepending a
    fig.basemap(region=overview_region, projection='M1.5i', frame='a0',
                X="a2.0i", Y="a-0.25i")
    fig.coast(frame="a5.0nwSE", resolution="l", shorelines="1/black",
              land="gray", water="white", X="a2.0i", Y="a-0.25i")
    print([region[0], region[1], region[1], region[0], region[0]])
    print([region[2], region[2], region[3], region[3], region[2]])
    fig.plot(
        x=np.array([region[0], region[1], region[1], region[0], region[0]]),
        y=np.array([region[2], region[2], region[3], region[3], region[2]]),
        pen="0.9p,red", X="a2.0i", Y="a-0.25i")
    # Plot zoom
    if zoom_region:
        if zoom_scale_length:
            zoom_scale = "f{0}/{1}/{2}/{3}+u".format(
                zoom_region[0] + 0.5 * (zoom_region[1] - zoom_region[0]),
                zoom_region[2] + 0.2 * (zoom_region[3] - zoom_region[2]),
                zoom_region[2], zoom_scale_length)
        fig.basemap(region=zoom_region, projection='M1.5i', frame='a0',
                    X="a2.0i", Y="a2.0i")
        if zoom_scale_length:
            fig.coast(frame="a0.05NwsE", resolution="l", shorelines="1/black",
                    land="white", water="white", X="a2.0i", Y="a2.0i",
                    L=zoom_scale)
        else:
            fig.coast(frame="a0.05NwsE", resolution="l", shorelines="1/black",
                    land="white", water="white", X="a2.0i", Y="a2.0i")
        # Plot stations
        if inventory:
            sta_x, sta_y = ([], [])
            for network in inventory:
                for station in network:
                    try:
                        sta_x.append(station.longitude)
                        sta_y.append(station.latitude)
                    except AttributeError:
                        print("Station {0} does not have a location".format(
                            station.code))
                        pass
            fig.plot(x=np.array(sta_x), y=np.array(sta_y), style='i0.45',
                    color="red", pen="black", X="a2.0i", Y="a2.0i")
        # Plot templates
        _mags = []
        for e in template_catalog:
            try:
                mag = e.preferred_magnitude().mag or e.magnitudes[0].mag
            except (IndexError, AttributeError):
                mag = 4.0
            _mags.append(mag)
        fig.plot(x=np.array([e.origins[0].longitude for e in template_catalog]),
                y=np.array([e.origins[0].latitude for e in template_catalog]),
                style='cc', C='depth.cpt', pen='black', X="a2.0i", Y="a2.0i",
                sizes=(np.array(_mags) / 8) ** 2,
                color=np.array([(e.origins[0].depth - min_depth) / 1000
                                for e in template_catalog]))
    # Plot depth scale
    with LibGMT() as lib:
        lib.call_module(
            "psscale", '-Cdepth.cpt -D{0}i/{1}i/{2}i/{3}h '
                       '-Bpx{4}+l"Depth (km)"'.format(
                0.75, 1.0, 1.0, 0.2, round((max_depth - min_depth) / 4)))
    os.remove("depth.cpt")
    if show:
        fig.show(method="external")
    return fig


def _reformat_grid_files(region):
    """
    Reformat grid files for the region (clip) and generate illumination grids.

    :type region: list
    :param region:
        list of [min-longitude, max-longitude, min-latitude, max-latitude]
    """
    if os.path.isfile('grid_file_tmp.grd'):
        os.remove('grid_file_tmp.grd')
    region_str = '/'.join([str(_) for _ in region])
    with LibGMT() as lib:
        lib.call_module(
            'grdclip', '{0}/data/NZ_resized_DEM.grd -R{1} -Ggrid_file_tmp.grd '
                       '-Sb0/NaN'.format(DIR, region_str))
        lib.call_module(
            'grdgradient', 'grid_file_tmp.grd -Gillumination.grd -A270 -Nt0.3')
        lib.call_module(
            'makecpt', '-Cgray -Z -T0/6000/500 -I > topo.cpt')
    return


def _clean_grid_files():
    for f in ['grid_file_tmp.grd', 'illumination.grd', 'topo.cpt']:
        if os.path.isfile(f):
            os.remove(f)


def gmt_multi_detection_plot(parties, catalog, data, plot_len=4, pre_det=0,
                             station="WHYM", channel="EHZ", fig=None,
                             show=False, x_offset=1.55, y_offset=1.5,
                             total_height=8, label_size=6, label_offset=1.5,
                             gap=15, catalog_diff=10):
    """
    Compare detections to those made by other methods.

    :type parties: dict
    :param parties:
        Dictionary keyed by method name, with items as
        eqcorrscan.core.match_filter.Party objects
    :type catalog: obspy.core.event.Catalog
    :param catalog: Original catalog of detections
    :type data: obspy.core.stream.Stream
    :param data: Data to extract detections from
    :type plot_len: float
    :param plot_len: Length of plotted streams in seconds
    :type station: str
    :param station: Station to plot - must be in data
    :type channel: str
    :param channel: Channel to plot from station - must be in data.
    :type fig: `gmt.Figure`
    :param fig: Figure to plot into
    :type show: bool
    :param show: Whether to show the figure or not
    :type x_offset: float
    :param x_offset: X-offset in inches for plot (from previous origin)
    :type y_offset: float
    :param y_offset: X-offset in inches for plot (from previous origin)
    """
    # Get list of unique detection-times
    parties_detect_times = {}
    for key, party in parties.items():
        if len(party) != 0:
            parties_detect_times.update({key: np.array([
                d.detect_time for f in party for d in f])})
    all_detections = []
    for d_times in parties_detect_times.values():
        all_detections.extend(d_times.tolist())
    all_detections.sort()
    uniq_detection_times = np.array([all_detections[0]])
    for d in all_detections:
        if not np.any(abs(uniq_detection_times - d) < gap):
            uniq_detection_times = np.append(
                uniq_detection_times, d)
    uniq_detection_times[::-1].sort()
    try:
        tr = data.select(station=station, channel=channel)[0]
    except IndexError:
        print("No data for {0}.{1}, no plot for you!".format(station, channel))
        return
    height = total_height / len(uniq_detection_times)
    label_loc = 'weSn'
    if not fig:
        fig = gmt.Figure()
    with LibGMT() as lib:
        lib.call_module('gmtset', 'FORMAT_GEO_MAP ddd.xx')
        lib.call_module('gmtset', 'MAP_FRAME_TYPE plain')
        lib.call_module('gmtset', 'FONT_ANNOT_PRIMARY 10p')
        lib.call_module('gmtset', 'FONT_LABEL 10p')
    for d_time in uniq_detection_times:
        detected_by = []
        # Work out which parties had this detection
        for key, d_times in parties_detect_times.items():
            if np.any(abs(d_times - d_time) < gap):
                detected_by.append(key)
                actual_d_time = d_times[
                    np.where(abs(d_times - d_time) < gap)[0][0]]
                detection = [d for f in parties[key] for d in f
                             if d.detect_time == actual_d_time][0]
        cut_tr = tr.slice(d_time - pre_det, d_time + plot_len - pre_det)
        x = np.arange(0, cut_tr.stats.npts * cut_tr.stats.delta,
                      cut_tr.stats.delta)
        y = cut_tr.data
        y = y - y.mean()
        y /= y.max()
        if len(x) > len(y):
            x = x[0:len(y)]
        if _detection_within_catalog(detection, catalog=catalog,
                                     max_diff=catalog_diff):
            color = "red"
        else:
            color = "black"
        fig.plot(
            x=x, y=y, J="X3i/{0}i".format(height), W="0.9p,{0}".format(color),
            R="{0}/{1}/{2}/{3}".format(0, x.max(), -1, 1),
            B=['px1.0+l"Time (s)"', 'py0.0+l"{0}"h'.format(d_time), label_loc],
            Y="a{0}i".format(y_offset), X="a{0}i".format(x_offset))
        # Do some ugly pstext work - output label to temporary file, then plot
        with open("text.tmp", "w") as f:
            f.write("{0} {1} {2}".format(
                label_offset, 0, d_time.strftime("%H:%M:%S")))
        with LibGMT() as lib:
            lib.call_module(
                "pstext", "text.tmp -F+f{0}p,black,- -Ya{1}i -Xa{2}i".format(
                    label_size, y_offset, 0))
        os.remove("text.tmp")
        x = 0.01 * plot_len
        for key, color in COLOR_IDS.items():
            if key in detected_by:
                fill = color
                fig.plot(
                    x=np.array([x]), y=np.array([0.0]), style='s0.25',
                    pen="0.9p,black", C=fill, Y="a{0}i".format(y_offset),
                    X="a{0}i".format(x_offset))
            x += 0.025 * plot_len
        y_offset += height
        label_loc = 'wesn'
    if show:
        fig.show(method="external")
    return fig


def gmt_template_plot(templates, station="WHYM", channel="EHZ", fig=None,
                      show=False, x_offset=1, y_offset=1.5, total_height=3.3):
    """
    Plot waveforms from a single matching template in all tribes - will plot
    first match found.

    :type templates: dict
    :param templates: Dictionary of templates to plot
    :type station: str
    :param station: Station to plot - must be in data
    :type channel: str
    :param channel: Channel to plot from station - must be in data.
    :type fig: `gmt.Figure`
    :param fig: Figure to plot into
    :type show: bool
    :param show: Whether to show the figure or not
    :type x_offset: float
    :param x_offset: X-offset in inches for plot (from previous origin)
    :type y_offset: float
    :param y_offset: X-offset in inches for plot (from previous origin)
    """
    if not fig:
        fig = gmt.Figure()
    label_loc = 'wESn'
    height = total_height / len(templates)
    for key, template in templates.items():
        try:
            tr = template.st.select(station=station, channel=channel)[0]
        except IndexError:
            print("No data for {0}.{1} in template: {2}".format(
                station, channel, key))
            continue
        x = np.arange(0, tr.stats.npts * tr.stats.delta, tr.stats.delta)
        y = tr.data / tr.data.max()
        fig.plot(
            x=x, y=y, J="X3i/{0}i".format(height),
            W="1.2p,{0}".format(COLOR_IDS[key]),
            R="{0}/{1}/{2}/{3}".format(0, x.max(), y.min(), y.max()),
            B=['px1.0+l"Time (s)"', 'py0.0+l"{0}"'.format(NAME_MAP[key]), label_loc],
            Y="a{0}i".format(y_offset), X="a{0}i".format(x_offset))
        y_offset += height
        label_loc = 'wEsn'
    if show:
        fig.show(method="external")
    return fig


def gmt_triple_plot(parties, station, channel, plot_len, data, catalog,
                    template_catalog, inventory, fm, templates, pre_det=0,
                    label_size=6, label_offset=1.5, region_pad=0.25,
                    scale_length=10, tick_spacing=0.25, fm_pad=0.1,
                    overview_region=[165.8, 179.4, -47.2, -34.4],
                    map_region=None, template_offset=4.7, template_height=3.3,
                    gap=20, catalog_diff=20, zoom_region=None, 
                    zoom_scale_length=None):
    fig = gmt_multi_detection_plot(
        parties=parties, station=station, channel=channel, plot_len=plot_len,
        data=data, catalog=catalog, fig=None, show=False, pre_det=pre_det,
        label_size=label_size, label_offset=label_offset, gap=gap,
        catalog_diff=catalog_diff)
    fig = gmt_map(template_catalog=template_catalog, fig=fig, show=False,
                  inventory=inventory, fm=fm, x_offset=5.0, y_offset=1.45,
                  region_pad=region_pad, scale_length=scale_length,
                  tick_spacing=tick_spacing, fm_pad=fm_pad,
                  overview_region=overview_region, region=map_region,
                  zoom_region=zoom_region, zoom_scale_length=zoom_scale_length)
    fig = gmt_template_plot(
        templates=templates, station=station, channel=channel, fig=fig,
        show=False, y_offset=template_offset, x_offset=0,
        total_height=template_height)
    return fig


def plot_detection_rate(parties, catalog_times=None, sta_lta=None, 
                        data=None, station="WHYM", channel="SHZ", gain=1):
    """
    Plot cumulative detections for multiple parties, optionally plot data
    envelope as well.

    :type parties: dict
    :param parties:
        Dictionary keyed by method name, with items as
        eqcorrscan.core.match_filter.Party objects
    :type catalog_times: list
    :param catalog_times: List of catalog detection times.
    :type data: obspy.core.stream.Stream
    :param data: Data to extract detections from
    :type station: str
    :param station: Station to plot - must be in data
    :type channel: str
    :param channel: Channel to plot from station - must be in data.
    """
    import matplotlib.dates as mdates
    from matplotlib.ticker import MaxNLocator
    label_map = {
        'gf': "Green's Func.", 'catalog': 'Tremor', 'synthetic': 'Naive',
        'real': "Real", "sta_lta": "STA/LTA"}
    nrows = 1
    if data:
        nrows += 1
    fig, axes = plt.subplots(nrows=nrows, ncols=1, sharex=True)
    if not data:
        axes = [axes]
    detect_times = {}
    for key, party in parties.items():
        if len(party) == 0:
            continue
        detect_times.update(
            {key: [d.detect_time for f in parties[key] for d in f]})
    if catalog_times:
        detect_times.update({'catalog': catalog_times})
    if sta_lta:
        detect_times.update({'sta_lta': sta_lta})
    max_counts = 0
    for key, _detect_times in detect_times.items():
        _detect_times.sort()
        try:
            first_det = _detect_times[0]
        except IndexError:
            continue
        _detect_times.insert(0, first_det)
        _detect_times = [d.datetime for d in _detect_times]
        try:
            color = COLOR_IDS[key]
        except KeyError:
            color = 'k'
        counts = np.arange(-1, len(_detect_times) - 1)
        if max(counts) > max_counts:
            max_counts = max(counts)
        axes[0].plot(_detect_times, counts, color=color, linewidth=2.0,
                     drawstyle='steps', label=label_map[key])
    axes[0].set_ylabel("Cumulative detections")
    axes[0].set_ylim(bottom=0)
    axes[0].yaxis.set_major_locator(MaxNLocator(integer=True))
    if data:
        env = data.select(
            station=station, channel=channel)[0].copy().filter(
            'bandpass', freqmin=1, freqmax=10, corners=2, zerophase=True)
        env.resample(20)
        starttime = env.stats.starttime
        endtime = env.stats.endtime
        delta = env.stats.delta
        env = envelope(env.data)
        # remove gain
        env /= gain
        # Use scientific notation
        max_str = "{0:.6E}".format(env.max())
        exponant = int(max_str.split("E")[-1])
        env /= 10 ** exponant
        times = np.arange(starttime.datetime, (endtime + delta).datetime,
                          timedelta(seconds=delta)).astype(datetime)
        axes[1].plot(times, env, 'k', label="Data Envelope")
        axes[1].set_ylabel("Velocity (m/s $10^{0}$)".format(
            "{" + str(exponant) + "}"))
        axes[1].set_xlim((min(times), max(times)))
        axes[1].set_ylim((0, max(env)))
        axes[1].ticklabel_format(style='sci', axis='y')
        #axes[0].xaxis.set_ticks([])
        plt.setp(axes[1].xaxis.get_majorticklabels(), rotation=30,
                 horizontalalignment='right')
    axes[-1].set_xlabel("Time")
    axes[-1].xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))
    axes[-1].xaxis.set_minor_formatter(mdates.DateFormatter("%H:%M"))
    axes[0].legend()
    plt.subplots_adjust(wspace=0, hspace=0)
    return fig


def overlain_plot(st1, st2, event=None):
    """
    Overlay waveforms and compare them.

    :param `obspy.core.stream.Stream` st1: Stream 1
    :param `obspy.core.stream.Stream` st2: Stream 2
    :param `obspy.core.event.Event` event:
        Event with picks appropriate for these streams.
    """
    pick_colors = {'P': 'k', 'S': 'b'}
    seed_ids = set([tr.id for st in [st1, st2] for tr in st])
    plot_starttime = min([tr.stats.starttime
                          for st in [st1, st2] for tr in st])
    fig, axes = plt.subplots(nrows=len(seed_ids), ncols=1, sharex=True)
    for i, seed_id in enumerate(sorted(list(seed_ids))):
        tr1 = st1.select(id=seed_id)
        tr2 = st2.select(id=seed_id)
        picks = [pick for pick in event.picks
                 if pick.waveform_id.get_seed_string() == seed_id]
        for tr, color in zip([tr1, tr2], ['k', 'r']):
            if len(tr) == 0:
                continue
            x = np.arange(0, len(tr[0]))
            x = x / tr[0].stats.sampling_rate
            x += tr[0].stats.starttime - plot_starttime
            y = tr[0].data
            y = y / y.max()
            axes[i].plot(x, y, c=color, linewidth=1)
        for pick in picks:
            axes[i].axvline(
                x=pick.time - plot_starttime,
                color=pick_colors[pick.phase_hint], linewidth=2,
                linestyle='--', label=pick.phase_hint)
        axes[i].set_ylabel(seed_id, rotation=0, labelpad=40)
    plt.setp([a.get_xticklabels() for a in fig.axes[:-1]],
             visible=False)
    fig.axes[-1].set_xlabel('Time (s)')
    fig.subplots_adjust(hspace=0)
    plt.setp([a.get_yticklabels() for a in fig.axes[:]], visible=False)
    plt.setp([a.get_yticklines() for a in fig.axes[:]], visible=False)
    plt.show()


def fmc_plot(catalog, detections=None, master=None, 
             fmc_dir="/home/calumch/my_programs/Built/FMC_1.01"):
    """
    Use FMC to plot a ternary diagram - needs FMC from:
        'https://josealvarezgomez.wordpress.com/2014/04/22/fmc-a-python-program-to-manage-classify-and-plot-focal-mechanism-data/',
    """
    import sys
    sys.path.insert(0, fmc_dir)
    from functionsFMC import pl2nd, nd2pt, pl2pl, ca2ax, nd2ar, kave
    from plotFMC import baseplot
    xplot, yplot,  = (np.zeros(len(catalog)), np.zeros(len(catalog)))
    
    for i, event in enumerate(catalog):
        str1 = event.focal_mechanisms[0].nodal_planes.nodal_plane_1.strike % 360
        dip1 = event.focal_mechanisms[0].nodal_planes.nodal_plane_1.dip % 360
        rake1 = event.focal_mechanisms[0].nodal_planes.nodal_plane_1.rake % 360
        if dip1 > 180:
            dip1 -= 180
        if dip1 > 90:
            dip1 = 180 - dip1
            str1 += 180
            str1 % 360
        anX, anY, anZ, dx, dy, dz = pl2nd(str1, dip1, rake1)
        px, py, pz, tx, ty, tz, bx, by, bz = nd2pt(anX, anY, anZ, dx, dy, dz)
        str2, dip2, rake2, dipdir2 = pl2pl(str1, dip1, rake1)
        trendp, plungp = ca2ax(px, py, pz)
        trendt, plungt = ca2ax(tx, ty, tz)
        trendb, plungb = ca2ax(bx, by, bz)
        x_kav, y_kav = kave(plungt, plungb, plungp)
        xplot[i] = x_kav
        yplot[i] = y_kav
    fig = baseplot("")
    ax = fig.gca()
    ax.scatter(xplot, yplot, s=2, c='white', alpha=0.7, linewidth=1.5,
               edgecolor='black')
    if detections is not None:
        detected_x = np.array([xplot[i] for i in range(len(catalog)) 
                               if detections[i]])
        detected_y = np.array([yplot[i] for i in range(len(catalog)) 
                               if detections[i]])
        ax.scatter(detected_x, detected_y, s=20, c='white', alpha=0.7,
                   linewidth=1.5, edgecolor='black')
    if master is not None:
        ax.scatter(xplot[master], yplot[master], s=20, c='red', alpha=0.7, 
                   linewidth=1.5, edgecolor='black')
    # fig = circles(xplot, yplot, Mwplot * 10, depplot, "")
    return fig


def single_beach_multi_mechanism_plot(catalog, detections=None,
                                      detect_vals=None, master=None,
                                      p_axis=True, t_axis=False):
    """
    Plot multiple mechanisms on one beachball plot.

    :type catalog: obspy.core.event.Catalog
    :param catalog:
        Catalog of events to plot, will plot the first focal mechanism found
        for each event, if there is one available
    :type detections: List
    :param detections:
        Whether or not detections were made for an event. detections[i]
        corresponds to catalog[i]
    :type master: Int
    :param master: Identifying index of main event in catalog
    """
    import matplotlib as mpl
    import mplstereonet

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='stereonet')
    detected_poles, undetected_poles = ({'x': [], 'y': [], 'c': []},
                                        {'x': [], 'y': []})
    for i, event in enumerate(catalog):
        _axis = False
        for nodal_plane_key in ['nodal_plane_1', 'nodal_plane_2']:
            try:
                nodal_plane = event.focal_mechanisms[0].nodal_planes.__dict__[nodal_plane_key]
            except IndexError:
                # No mechanism
                continue
            except KeyError:
                print("{0} not found".format(nodal_plane_key))
                continue
            try:
                strike = nodal_plane.strike
            except AttributeError:
                continue
            nodal_plane_2 = aux_plane(nodal_plane)
            pa = meca_dc2axe(nodal_plane, nodal_plane_2)
            if not _axis:
                if p_axis:
                    if detections[i]:
                        detected_poles['x'].append(pa.p_axis.plunge)
                        detected_poles['y'].append(pa.p_axis.azimuth)
                        detected_poles['c'].append(detect_vals[i])
                    else:
                        undetected_poles['x'].append(pa.p_axis.plunge)
                        undetected_poles['y'].append(pa.p_axis.azimuth)
                elif t_axis:
                    if detections[i]:
                        detected_poles['x'].append(pa.t_axis.plunge)
                        detected_poles['y'].append(pa.t_axis.azimuth)
                        detected_poles['c'].append(detect_vals[i])
                    else:
                        undetected_poles['x'].append(pa.t_axis.plunge)
                        undetected_poles['y'].append(pa.t_axis.azimuth)
                _axis = True
    ax.line(undetected_poles['x'], undetected_poles['y'], markerfacecolor='k',
            markeredgecolor='k', markersize=1)
    norm = mpl.colors.Normalize(vmin=min(detected_poles['c']),
                                vmax=max(detected_poles['c']))    
    colors = CMAP(norm(detected_poles['c']))
    for x, y, c in zip(detected_poles['x'], detected_poles['y'], colors):
        ax.line(x, y, color=c, markerfacecolor=c, markersize=4)
    ax1 = plt.axes([0.85, 0.1, 0.075, 0.8])
    cb1 = mpl.colorbar.ColorbarBase(
        ax1, cmap=CMAP, norm=norm, orientation='vertical')
    cb1.set_label("Cross-correlation sum")
    # ax.line(detected_poles['x'], detected_poles['y'], 
    #         markerfacecolor=CMAP(detected_poles['c']), markersize=4)
    if master:
        master_event = catalog[master]
        plotted_p_axis = False
        for nodal_plane_key in ['nodal_plane_1', 'nodal_plane_2']:
            try:
                nodal_plane = master_event.focal_mechanisms[0].nodal_planes.__dict__[nodal_plane_key]
            except IndexError:
                # No mechanism
                continue
            except KeyError:
                print("{0} not found".format(nodal_plane_key))
                continue
            try:
                strike = nodal_plane.strike
            except AttributeError:
                continue            
            if p_axis:
                if not plotted_p_axis:
                    # pa = np_to_principal_axes(nodal_plane)
                    nodal_plane_2 = aux_plane(nodal_plane)
                    pa = meca_dc2axe(nodal_plane, nodal_plane_2)
                    ax.line(pa.p_axis.plunge, pa.p_axis.azimuth, 'o', 
                            markerfacecolor='cyan', markersize=5,
                            markeredgecolor='k')
            if t_axis:
                if not plotted_p_axis:
                    # pa = np_to_principal_axes(nodal_plane)
                    nodal_plane_2 = aux_plane(nodal_plane)
                    pa = meca_dc2axe(nodal_plane, nodal_plane_2)
                    ax.line(pa.t_axis.plunge, pa.t_axis.azimuth, 'o', 
                            markerfacecolor='cyan', markersize=5,
                            markeredgecolor='k')
    ax.grid()
    return fig


def multi_beach_plot(catalog, detections=None, master=None):
    """
    Plot multiple mechanisms.

    :type catalog: obspy.core.event.Catalog
    :param catalog:
        Catalog of events to plot, will plot the first focal mechanism found
        for each event, if there is one available
    :type detections: List
    :param detections:
        Whether or not detections were made for an event. detections[i]
        corresponds to catalog[i]
    :type master: Int
    :param master: Identifying index of main event in catalog
    """
    width = 1000
    plot_width = width * 0.95
    ncols = round(len(catalog) ** 0.5)
    nrows = ncols
    if nrows * ncols < len(catalog):
        nrows += 1
    fig, axes = plt.subplots(nrows=nrows, ncols=ncols)
    if nrows * ncols == 1:
        axes = [axes]
    if ncols > 1:
        axes = axes.flatten()
    fig.subplots_adjust(left=0, bottom=0, right=1, top=1)
    fig.set_figheight(width // 100)
    fig.set_figwidth(width // 100)
    for i, event in enumerate(catalog):
        ax = axes[i]
        try:
            nodal_plane = event.focal_mechanisms[0].nodal_planes.nodal_plane_1
        except IndexError:
            continue
        fm = [nodal_plane.strike, nodal_plane.dip, nodal_plane.rake]
        if detections:
            if detections[i]:
                color = 'r'
                alpha = 1.0
            else:
                color = 'k'
                alpha = 0.2
        else:
            color = next(COLORS)
            alpha = 0.8
        if master and i == master:
            bgcolor = 'k'
        else:
            bgcolor = 'w'
        collection = beach(
            fm, linewidth=2, edgecolor=color, bgcolor=bgcolor, alpha=alpha,
            nofill=False, xy=(0, 0), width=plot_width, size=width / 2,
            zorder=i)
        ax.add_collection(collection)
        ax.autoscale_view(tight=False, scalex=True, scaley=True)
    for ax in axes:
        ax.axison = False
    return fig


def sine_bounded(angle):
    """Convert angle to be within 0-360"""
    return angle % 360


def fm_ternary_plot(catalog, detections=None, master=None):
    """
    Plot of multiple mechanisms on a ternary plot a la Frohlich.

    :type catalog: obspy.core.event.Catalog
    :param catalog:
        Catalog of events to plot, will plot the first focal mechanism found
        for each event, if there is one available
    :type detections: List
    :param detections:
        Whether or not detections were made for an event. detections[i]
        corresponds to catalog[i]
    :type master: Int
    :param master: Identifying index of main event in catalog
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    detected = {'x': [], 'y': []}
    not_detected = {'x': [], 'y': []}
    principal_axes = []
    for i, event in enumerate(catalog):
        for nodal_plane_key in ['nodal_plane_1', 'nodal_plane_2']:
            try:
                nodal_plane = event.focal_mechanisms[0].nodal_planes.__dict__[nodal_plane_key]
            except IndexError:
                # No mechanism
                continue
            except KeyError:
                print("{0} not found".format(nodal_plane_key))
                continue
            try:
                strike, dip, rake = (
                    nodal_plane.strike, nodal_plane.dip, nodal_plane.rake)
            except AttributeError:
                continue
            x, y, pa = nodal_plane_to_frolich(strike=strike, dip=dip, rake=rake)
            principal_axes.append(pa)
            if not detections[i]:
                not_detected['x'].append(x)
                not_detected['y'].append(y)
            else:
                detected['x'].append(x)
                detected['y'].append(y)
    ax.scatter(not_detected['x'], not_detected['y'], facecolors='k',           
               edgecolors='none', s=1, marker='o')
    ax.scatter(detected['x'], detected['y'], facecolors='cyan',
               edgecolors='k', s=4, marker='o')  
    if master:
        event = catalog[master]
        for nodal_plane_key in ['nodal_plane_1', 'nodal_plane_2']:
            try:
                nodal_plane = event.focal_mechanisms[0].nodal_planes.__dict__[nodal_plane_key]
            except IndexError:
                # No mechanism
                continue
            except KeyError:
                print("{0} not found".format(nodal_plane_key))
                continue
            try:
                strike, dip, rake = (
                    nodal_plane.strike, nodal_plane.dip, nodal_plane.rake)
            except AttributeError:
                continue
            x, y = nodal_plane_to_frolich(strike=strike, dip=dip, rake=rake)
            ax.scatter(x, y, 'ro', s=5, edgecolor='k')
    return fig


def nodal_plane_to_frolich(strike, dip, rake):
    # Bound dip between -90-90
    nodal_plane = NodalPlane(strike=strike % 360, dip=dip % 180, rake=rake % 360)
    nodal_plane_2 = aux_plane(nodal_plane)
    pa = meca_dc2axe(nodal_plane, nodal_plane_2)
    thi = atand(sind(pa.t_axis.plunge) / sind(pa.p_axis.plunge)) - math.radians(45)
    x = (cosd(pa.n_axis.plunge) * sind(thi) /
            (sind(35.26) * sind(pa.n_axis.plunge) + 
            cosd(35.26) * sind(pa.n_axis.plunge) * cosd(thi)))
    y = (cosd(35.26) * sind(pa.n_axis.plunge) - sind(35.26) * cosd(pa.n_axis.plunge) * cosd(thi) /
            sind(35.26) * sind(pa.n_axis.plunge) + cosd(35.26) * sind(pa.n_axis.plunge) * cosd(thi))
    return x, y, pa


def location_sensitivity_plot(party, tribe, inventory, master, latlon=True,
                              show=False, plot_template_locs=True):
    """
    4-panel plot inc. 3-D plot of which templates detected and which did not.

    :type party: eqcorrscan.core.match_filter.Party
    :param party: Party of detections made
    :type tribe: eqcorrscan.core.match_filter.Tribe
    :param tribe: Tribe of templates used
    :type inventory: obspy.core.inventory.Inventory
    :param inventory: Stations used
    :type master: obspy.core.event.Event
    :param master: Master event that we are trying to detect.
    """
    master_ori = master.preferred_origin() or master.origins[0]
    master_loc = Geographic(
        latitude=master_ori.latitude, longitude=master_ori.longitude,
        depth=master_ori.depth / -1000, time=master_ori.time.datetime,
        magnitude=master.magnitudes[0].mag)
    template_locs = []
    for template in tribe:
        template_loc = Geographic(
            latitude=template.event.origins[0].latitude,
            longitude=template.event.origins[0].longitude,
            depth=template.event.origins[0].depth / -1000)
        template_locs.append(template_loc)
    detection_locs = []
    detection_ccc_sums = []
    for family in party:
        if len(family.detections) == 0:
            continue
        template_loc = Geographic(
            latitude=family.template.event.origins[0].latitude,
            longitude=family.template.event.origins[0].longitude,
            depth=family.template.event.origins[0].depth / -1000)
        detection_locs.append(template_loc)
        detection_ccc_sums.append(family.detections[0].detect_val)
    best_det_loc = detection_locs[detection_ccc_sums.index(max(detection_ccc_sums))]
    station_locs = []
    for network in inventory:
        for station in network:
            station_loc = Geographic(
                latitude=station.latitude, longitude=station.longitude,
                depth=station.elevation / 1000)
            station_locs.append(station_loc)
    east_label = "Longitude (deg)"
    north_label = "Latitude (deg)"
    if not latlon:
        east_label = "East(km)"
        north_label = "North (km)"
        # Convert locations to meters
        template_locs = [
            template_loc.to_xyz(origin=master_loc, strike=0, dip=90)
            for template_loc in template_locs]
        detection_locs = [
            detection_loc.to_xyz(origin=master_loc, strike=0, dip=90)
            for detection_loc in detection_locs]
        station_locs = [
            station_loc.to_xyz(origin=master_loc, strike=0, dip=90)
            for station_loc in station_locs]
        master_loc = master_loc.to_xyz(origin=master_loc, strike=0, dip=90)
        best_det_loc = best_det__loc.to_xyz(origin=master_loc, strike=0, dip=90)
    # Generate a four panel subplot
    fig = plt.figure()
    ax1 = fig.add_subplot(2, 2, 1)
    ax2 = fig.add_subplot(2, 2, 2)
    ax3 = fig.add_subplot(2, 2, 3)
    ax4 = fig.add_subplot(2, 2, 4, projection='3d')
    loc_dict = {'station': station_locs, 'template': template_locs,
                'detection': detection_locs, 'master': [master_loc],
                'best_det': [best_det_loc]}
    marker_dict = {
        'station': 
            {'facecolors': 'b', 'edgecolors': 'k', 'size': 20, 'marker': '^'},
        'template': 
            {'facecolors': 'none', 'edgecolors': 'b', 'size': 1, 'marker': 'o'},
        'detection': 
            {'facecolors': 'none', 'c': np.array(detection_ccc_sums), 
             'edgecolors': 'k', 'size': 3, 'marker': 'o'},
        'master': 
            {'facecolors': 'k', 'edgecolors': 'k', 'size': 30, 'marker': 'o'},
        'best_det': 
            {'facecolors': 'w', 'edgecolors': 'k', 'size': 30, 'marker': 'o'},
        }
    for i, key in enumerate(['station', 'template', 'master', 'detection', 'master', 'best_det']):
        locations = loc_dict[key]
        if not plot_template_locs and key == 'template':
            continue
        x, y, z = ([0] * len(locations), [0] * len(locations),
                [0] * len(locations))
        for i, location in enumerate(locations):
            if isinstance(location, Geographic):
                x[i] = location.longitude
                y[i] = location.latitude
                z[i] = location.depth * -1
            elif isinstance(location, Location):
                x[i] = location.x
                y[i] = location.y
                z[i] = location.z * -1
        # Map view
        plt.sca(ax1)
        fig, scatter = plot_xy(
            x, y, fig=fig, axis_labels=(east_label, north_label),
            **marker_dict[key])
        # X-section1
        plt.sca(ax3)
        fig, scatter = plot_xy(
            x, z, fig=fig, axis_labels=(east_label, "Depth(km)"),
            **marker_dict[key], invert_y=True)
        # X-section2
        plt.sca(ax2)
        fig, scatter = plot_xy(
            z, y, fig=fig, axis_labels=("Depth (km)", north_label),
            **marker_dict[key])
        ax2.yaxis.tick_right()
        ax2.yaxis.set_label_position("right")
        # Make the 3D plot
        plt.sca(ax4)
        fig = plot_xyz(
            locations=locations, fig=fig, zorder=i, **marker_dict[key])
    ax4.view_init(elev=32, azim=151)
    # fig.colorbar(mappable=scatter, ax=ax4)
    if show:
        plt.show()
    return fig


def plot_xy(x, y, facecolors, edgecolors, size, marker,
            axis_labels=(None, None), fig=None, c=None, 
            invert_y=False, invert_x=False):
    """ Make 2D scatter plots of locations """
    if not fig:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
    else:
        ax = fig.gca()
    if c is not None:
        mappable = ax.scatter(x, y, c=c, marker=marker, s=size**2, cmap=CMAP)
    else:
        mappable = ax.scatter(x, y, facecolors=facecolors, 
                              edgecolors=edgecolors, marker=marker, s=size)
    ax.set_xlabel(axis_labels[0])
    ax.set_ylabel(axis_labels[1])
    if invert_y:
        ax.invert_yaxis()
    if invert_x:
        ax.invert_xaxis()
    return fig, mappable


def plot_xyz(locations, facecolors, edgecolors, size, marker, zorder=0,
             fig=None, c=None):
    """
    Make a 3D scatter plot of locations, either list of Location, or list of
    tuples of (x, y, z).
    """
    x, y, z = ([0] * len(locations), [0] * len(locations),
               [0] * len(locations))
    for i, location in enumerate(locations):
        if isinstance(location, Geographic):
            x[i] = location.longitude
            y[i] = location.latitude
            z[i] = location.depth * -1
        elif isinstance(location, Location):
            x[i] = location.x
            y[i] = location.y
            z[i] = location.z * -1
        else:
            raise TypeError("Location is neither Geographic nor Location")
    if not fig:
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
    else:
        ax = fig.gca()
    if c is not None:
        s = ax.scatter(x, y, z, c=c, marker=marker, s=size**2, zorder=zorder,
                       cmap=CMAP)
        cax = fig.add_axes([0.9, 0.01, 0.025, 0.45])
        cb = fig.colorbar(s, cax=cax, orientation="vertical")
        cb.set_label("Cross-correlation sum")
    else:
        s = ax.scatter(x, y, z, facecolors=facecolors, edgecolors=edgecolors,
                       marker=marker, s=size, zorder=zorder)
    s.set_edgecolors = s.set_facecolors = lambda *args:None
    if isinstance(locations[0], Geographic):
        ax.set_xlabel("Longitude (deg)")
        ax.set_ylabel("Latitude (deg)")
        ax.set_zlabel("Depth (km)")
    else:
        ax.set_xlabel('East (km)')
        ax.set_ylabel('North (km)')
        ax.set_zlabel('Depth (km)')
    ax.invert_zaxis()
    return fig

"""
    Utility functions for synthetic examples.

    :author:    Calum J. Chamberlain
    :data:      18/10/2017

    Licence:
    --------

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import numpy as np
import sys
import time
import threading
import os
import socket

from obspy.core.event import (
    CreationInfo, ResourceIdentifier, WaveformStreamID, Pick, Catalog)
from obspy.taup import TauPyModel

from io import StringIO

from eqcorrscan import Party, Tribe


EARTHRADIUS = 6371  # Global definition of earth radius in km.

if socket.gethostname() == 'XPS-13-9370':
    AXITRA_PATH = "/home/calumch/Desktop/axitra_testing/axitra"
else:
    AXITRA_PATH = "/Users/home/chambeca/axitra_testing/axitra"


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        self._stderr = sys.stderr
        sys.stdout = self._stringio = StringIO()
        sys.stderr = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout
        sys.stderr = self._stderr


class Spinner:
    busy = False
    delay = 0.1

    @staticmethod
    def spinning_cursor():
        while 1:
            for cursor in '|/-\\':
                yield cursor

    def __init__(self, delay=None):
        self.spinner_generator = self.spinning_cursor()
        if delay and float(delay):
            self.delay = delay

    def spinner_task(self):
        while self.busy:
            sys.stdout.write(next(self.spinner_generator))
            sys.stdout.flush()
            time.sleep(self.delay)
            sys.stdout.write('\b')
            sys.stdout.flush()

    def start(self):
        self.busy = True
        threading.Thread(target=self.spinner_task).start()

    def stop(self):
        self.busy = False
        time.sleep(self.delay)


def _detection_within_catalog(detection, catalog, max_diff=2):
    """
    Check if a detection is within an already existing catalog
    :param detection: detection object
    :param catalog: catalog object to check within
    :param max_diff: Maximum difference in time for
    :return: bool
    """
    time_diffs = []
    for event in catalog:
        diffs = []
        for pick in detection.event.picks:
            for ev_pick in event.picks:
                if _pick_match(pick, ev_pick):
                    diffs.append(abs(pick.time - ev_pick.time))
        if len(diffs) > 0:
            time_diffs.append(np.mean(diffs))
    if len(time_diffs) == 0:
        print('No matching picks')
        return False
    if min(time_diffs) < max_diff:
        return True
    return False


def _pick_match(pick_1, pick_2):
    if pick_1.waveform_id.station_code != pick_2.waveform_id.station_code:
        return False
    if pick_1.waveform_id.channel_code != pick_2.waveform_id.channel_code:
        return False
    return True


def detect_in_data(tribe, data, verbose=False, mad_mult=8, daylong=True,
                   decluster=True, parallel_process=True, cores=None):
    """
    Detect within the dataset.

    :type tribe: eqcorrscan.core.match_filter.Tribe
    :param tribe: Tribe to use for detection.
    :return: eqcorrscan.core.match_filter.Party of detections
    """
    # These data are an annoying length that slows processing down
    if not verbose:
        with Capturing() as _:
            party = tribe.detect(
                stream=data, threshold_type='MAD', threshold=mad_mult,
                trig_int=3, plotvar=False, process_cores=2,
                parallel_process=parallel_process, daylong=daylong,
                xcorr_func="fftw", concurrency="concurrent", cores=cores)
            if decluster:
                try:
                    party.decluster(trig_int=2, metric='cor_sum')
                except IndexError:
                    pass
    else:
        party = tribe.detect(
            stream=data, threshold_type='MAD', threshold=mad_mult, trig_int=3,
            plotvar=False, parallel_process=parallel_process,
            daylong=daylong, xcorr_func="fftw", concurrency="concurrent",
            cores=cores, process_cores=2)
        if decluster:
            try:
                party.decluster(trig_int=2, metric='cor_sum')
            except IndexError:
                pass
    if not decluster:
        # Only return parties with detections
        resulting_party = Party()
        for family in party:
            if len(family.detections) > 0:
                resulting_party += family
        party = resulting_party
    return party


def snr_threshold(party, min_snr, data, window=(0, 4)):
    """
    Remove detections below some signal-to-noise ratio.

    :param party: Party to remove detections from
    :param min_snr: float to define the minimum snr allowed
    """
    for family in party:
        high_snr_detections = []
        for detection in family:
            mean_snr = 0
            n_chans = 0
            for station, channel in detection.chans:
                try:
                    win_data = data.select(
                        station=station, channel=channel).slice(
                            detection.detect_time + window[0],
                            detection.detect_time + window[1])[0].data
                    mean_snr += np.max(win_data) / np.sqrt(np.mean(
                        np.square(win_data)))
                    n_chans += 1
                except IndexError:
                    pass
            mean_snr /= n_chans
            if mean_snr > min_snr:
                high_snr_detections.append(detection)
        family.detections = high_snr_detections


def _log_print(string, sep):
    sys.stdout.write(sep * 80)
    sys.stdout.write('\n')
    sys.stdout.write(string.center(80))
    sys.stdout.write('\n')
    sys.stdout.write(sep * 80)
    sys.stdout.write('\n')
    sys.stdout.flush()
    return


def seis_sim(sp, amp_ratio=1.5, flength=False, phaseout='all', sine_samp=0.5):
    """
    Generate a simulated seismogram from a given S-P time.

    Will generate spikes separated by a given S-P time, which are then
    convolved with a decaying sine function.  The P-phase is simulated by a
    positive spike of value 1, the S-arrival is simulated by a decaying
    boxcar of maximum amplitude 1.5.  These amplitude ratios can be altered by
    changing the amp_ratio, which is the ratio S amplitude:P amplitude.

    .. note::
        In testing this can achieve 0.3 or greater cross-correlations with
        data.

    :type sp: int
    :param sp: S-P time in samples
    :type amp_ratio: float
    :param amp_ratio: S:P amplitude ratio
    :type flength: int
    :param flength: Fixed length in samples, defaults to False
    :type phaseout: str
    :param phaseout:
        Either 'P', 'S' or 'all', controls which phases to cut around, defaults
        to 'all'. Can only be used with 'P' or 'S' options if flength
        is set.

    :returns: Simulated data.
    :rtype: :class:`numpy.ndarray`
    """
    if flength and 2.5 * sp < flength and 100 < flength:
        additional_length = flength
    elif 2.5 * sp < 100.0:
        additional_length = 100
    else:
        additional_length = 2.5 * sp
    synth = np.zeros(int(sp + 10 + additional_length))
    # Make the array begin 10 samples before the P
    # and at least 2.5 times the S-P samples after the S arrival
    synth[10] = 1.0  # P-spike fixed at 10 samples from start of window
    # The length of the decaying S-phase should depend on the SP time,\
    # Some basic estimations suggest this should be atleast 10 samples\
    # and that the coda should be about 1/10 of the SP time
    s_length = 10 + int(sp // 3)
    s_spikes = np.arange(amp_ratio, 0, -(amp_ratio / s_length))
    # What we actually want, or what appears better is to have a series of\
    # individual spikes, of alternating polarity...
    for i in range(len(s_spikes)):
        if i in np.arange(1, len(s_spikes), 2):
            s_spikes[i] = 0
        if i in np.arange(2, len(s_spikes), 4):
            s_spikes[i] *= -1
    # Put these spikes into the synthetic
    synth[10 + sp:10 + sp + len(s_spikes)] = s_spikes
    # Generate a rough damped sine wave to convolve with the model spikes
    sine_x = np.arange(0, 10.0, sine_samp)
    damped_sine = np.exp(-sine_x) * np.sin(2 * np.pi * sine_x)
    # Convolve the spike model with the damped sine!
    synth = np.convolve(synth, damped_sine)
    # Normalize snyth
    synth /= np.max(np.abs(synth))
    if not flength:
        return synth
    else:
        if phaseout in ['all', 'P']:
            synth = synth[0:flength]
        elif phaseout == 'S':
            synth = synth[sp:]
            if len(synth) < flength:
                # If this is too short, pad
                synth = np.append(synth, np.zeros(flength - len(synth)))
            else:
                synth = synth[0:flength]
    return synth


def axitra_sim(event, inventory, velocities, wavelet='ricker',
               wavelet_period=1.0, output='velocity', path=None, verbose=False,
               index=0, offset=-8, apply_response=True):
    """
    Simulate waveforms using the axitra synthetic Greens function code.

    axitra must be built and on your path. The source code for convm must be
    replaced with the convm.f file in this directory, which allows command
    line arguments to be passed.

    axitra can be obtained from:
     `https://www.isterre.fr/docrestreint.api/9067/467e17c679d2831d9448463fee6cb66696f5c739/tgz/axitra.tgz`

    :param event:
        obspy.core.event.Event including location and moment tensor information
    :param inventory:
        obspy.core.event.Inventory of stations to be simulated (only needs
        location information)
    :param list velocities:
        List of tuples of (depth to base, vp, vs, rho, Qp, Qs) in m and m/s
    :param str wavelet: wavelet to convolve with axitra Green's functions
    :param float wavelet_period:
        Period of wavelet to convolve with Green's functions
    :param str output:
        Output, must be one of ('displacement', 'velocity', 'acceleration')
    :param path: optional path to axitra binaries.
    :param index: Index to append to temporary directory

    :return: obspy.core.stream.Stream

    """
    import subprocess
    import os
    import shutil
    from obspy import read

    orientation_map = {'X': ['E', '2'], 'Y': ['N', '1'], 'Z': ['Z']}
    wavelet_map = {'ricker': '1', 'dirac': '0', 'step': '2', 'stored': '3',
                   'triangle': '4', 'ramp': '5', 'haskell': '6',
                   'true-step': '7', 'trapezoid': '8'}
    output_map = {'displacement': '1', 'velocity': '2', 'acceleration': '3'}

    axitra = "axitra"
    convms = "convms"
    if path:
        axitra = os.path.join(path, axitra)
        convms = os.path.join(path, convms)

    os.makedirs(".axitra_temp{0}".format(index))
    os.chdir(".axitra_temp{0}".format(index))

    with open("source", "w") as source_f:
        origin = event.preferred_origin() or event.origins[0]
        source_f.write("%i\t%f\t%f\t%f\n" % (0, origin.latitude,
                                             origin.longitude, origin.depth))
    with open("axi.hist", "w") as axi_f:
        fm = event.focal_mechanisms[0]
        axi_f.write("%i %s %s %s %s 0. 0. 10.0\n" %
                    (0, fm.moment_tensor.scalar_moment,
                     fm.nodal_planes.nodal_plane_1.strike,
                     fm.nodal_planes.nodal_plane_1.dip,
                     fm.nodal_planes.nodal_plane_1.rake))

    station_map = {}
    network_map = {}
    channel_map = {}
    with open("station", "w") as sta_f:
        i = 0
        for network in inventory:
            for station in network:
                station_map.update({str(i).zfill(3): station.code})
                network_map.update({str(i).zfill(3): network.code})
                chan_codes = []
                for channel in station:
                    chan_codes.append((channel.location_code, channel.code))
                channel_map.update({str(i).zfill(3): chan_codes})
                sta_f.write("\t%i\t%f\t%f\t%f\n" %
                            (i, station.latitude, station.longitude,
                             station.elevation))
                i += 1

    with open("axi.data", "w") as axi_data_f:
        axi_data_f.write(
            " &input\n"
            "nc=%i,nfreq=1024,tl=70.,aw=2.,nr=%i,ns=1,xl=1000000.,ikmax=100000,\n"
            "latlon=.true.,sourcefile=\"source\",statfile=\"station\"\n"
            "//\n" % (len(velocities), i))
        """
        Free format of:
        thickness vp vs rho qp qs
        """
        prev_depth = 0
        for layer in velocities:
            axi_data_f.write("  %f %f %f %f %f %f\n" %
                             (layer[0] - prev_depth, layer[1], layer[2],
                              layer[3], layer[4], layer[5]))
            prev_depth = layer[0]

    if not verbose:
        with Capturing() as _output:
            # Run axitra
            subprocess.run([axitra], stdout=subprocess.PIPE)
            # Run convms
            subprocess.run([convms, wavelet_map[wavelet], str(wavelet_period),
                            output_map[output]], stdout=subprocess.PIPE)
    else:
        # Run axitra
        subprocess.call([axitra])
        # Run convms
        subprocess.call([convms, wavelet_map[wavelet], str(wavelet_period),
                         output_map[output]])

    st = read("*.sac")
    st.sort(['station', 'channel'])
    for tr in st:
        _origin = event.preferred_origin() or event.origins[0]
        tr.stats.starttime = _origin.time
        tr.stats.starttime += offset
        tr.stats.network = network_map[tr.stats.station]
        # Map X to E, Y to N and Z to Z
        channels = channel_map[tr.stats.station]
        try:
            channel = [chan for chan in channels
                       if chan[1][-1] in orientation_map[tr.stats.channel]][0]
        except IndexError:
            st.remove(tr)
            continue
        tr.stats.location = channel[0]
        tr.stats.channel = channel[1]
        tr.stats.station = station_map[tr.stats.station]
        if apply_response:
            try:
                paz = inventory.get_response(
                    seed_id=tr.id, datetime=origin.time).get_paz()
            except Exception as e:
                print("Response not applied:")
                print(e)
            if paz:
                tr.simulate(paz_simulate={
                    'poles': paz._poles,
                    'zeros': paz._zeros,
                    'gain': paz.stage_gain}, simulate_sensitivity=False)
    os.chdir("..")
    shutil.rmtree(".axitra_temp{0}".format(index))
    return st


def get_geonet_moment_tensor(catalog, verbose=False, use_centroid=False):
    """
    Get the moment tensor info for a given set of events, and add them to the
    events.

    Use John Ristau's MT catalog here:
        `https://raw.githubusercontent.com/GeoNet/data/master/moment-tensor/GeoNet_CMT_solutions.csv`
    which does not include all events, if events are not in the database they
    will warn that they are not found.

    :param catalog: obspy.core.event.Event to get moment tensor for.

    .. Note::
        Works in-place on given event.
    """
    import urllib.request
    import csv

    from obspy.core.event import (
        FocalMechanism, MomentTensor, Origin, CreationInfo, ResourceIdentifier,
        NodalPlane, NodalPlanes, PrincipalAxes, Axis, Magnitude, Tensor)

    # Build local database
    response = urllib.request.urlopen(
        "https://raw.githubusercontent.com/GeoNet/data/master/moment-tensor/"
        "GeoNet_CMT_solutions.csv")
    reader = csv.DictReader(response.read().decode('utf-8').splitlines())

    moment_tensors = [line for line in reader]
    for event in catalog:
        mt = [_mt for _mt in moment_tensors
              if _mt['PublicID'] == event.resource_id.id.split('/')[-1]]
        if len(mt) == 0:
            if verbose:
                print("Event %s not found in database, skipping" %
                      event.resource_id.id.split('/')[-1])
            continue
        mt = mt[0]
        preferred_origin = event.preferred_origin() or event.origins[0]
        fm_origin = Origin(
            latitude=float(mt['Latitude']), longitude=float(mt['Longitude']),
            depth=float(mt['CD']) * 1000,
            time=preferred_origin.time,  # Note, John's origins are not precise
            origin_type="centroid", evaluation_mode="automatic",
            creation_info=CreationInfo(agency_id="GNS", author="John Ristau"),
            method_id=ResourceIdentifier("GNS_MT_solution"))
        local_mag = Magnitude(mag=float(mt['ML']), magnitude_type="ML",
                              origin_id=fm_origin.resource_id)
        moment_mag = Magnitude(mag=float(mt['Mw']), magnitude_type="MW",
                               origin_id=fm_origin.resource_id)
        event.origins.append(fm_origin)
        if use_centroid:
            event.preferred_origin_id = fm_origin.resource_id
        else:
            event.preferred_origin_id = event.origins[0].resource_id
        event.magnitudes.append(local_mag)
        event.magnitudes.append(moment_mag)

        fm = FocalMechanism(
            triggering_origin_id=fm_origin.resource_id,
            nodal_planes=NodalPlanes(
                nodal_plane_1=NodalPlane(strike=float(mt['strike1']),
                                         dip=float(mt['dip1']),
                                         rake=float(mt['rake1'])),
                nodal_plane_2=NodalPlane(strike=float(mt['strike2']),
                                         dip=float(mt['dip2']),
                                         rake=float(mt['rake2'])),
                preferred_plane=1),
            principal_axes=PrincipalAxes(
                t_axis=Axis(azimuth=float(mt['Taz']), plunge=float(mt['Tpl']),
                            length=mt['Tva']),
                p_axis=Axis(azimuth=float(mt['Paz']), plunge=float(mt['Ppl']),
                            length=mt['Pva']),
                n_axis=Axis(azimuth=float(mt['Naz']), plunge=float(mt['Npl']),
                            length=mt['Nva'])),
            station_polarity_count=int(mt['NS']),
            method_id=ResourceIdentifier("GNS_MT_solution"),
            moment_tensor=MomentTensor(
                derived_origin_id=fm_origin.resource_id,
                moment_magnitude_id=moment_mag.resource_id,
                scalar_moment=_dyne_cm_to_nm(float(mt['Mo'])),
                variance_reduction=float(mt['VR']),
                double_couple=float(mt['DC']) / 100,
                tensor=Tensor(
                    m_rr=_dyne_cm_to_nm(float(mt['Mxx']) * 1e20),
                    m_tt=_dyne_cm_to_nm(float(mt['Myy']) * 1e20),
                    m_pp=_dyne_cm_to_nm(float(mt['Mzz']) * 1e20),
                    m_rt=_dyne_cm_to_nm(float(mt['Mxy']) * 1e20),
                    m_rp=_dyne_cm_to_nm(float(mt['Mxz']) * 1e20),
                    m_tp=_dyne_cm_to_nm(float(mt['Myz']) * 1e20)),
                creation_info=CreationInfo(agency_id="GNS",
                                           author="John Ristau")))
        event.focal_mechanisms.append(fm)
    return catalog


def _dyne_cm_to_nm(value):
    """
    Convert value from dyne cm to Nm.
    """
    return value / 1e7


def predict_pick_times(event, inventory, only_picked=True):
    """
    Predict pick times for a given network and source geometry.

    :type event: obspy.core.event.Event
    :param event: Event, with picks to be copied
    :type inventory: obspy.core.station.Inventory
    :param inventory: Inventory of stations to predict arrival times from.

    :return: obspy.core.event.Event with updated picks.
    """
    model = TauPyModel(
        model=os.path.join(os.path.dirname(__file__), "data/Ristau.npz"))
    event_out = event.copy()
    event_out.picks = []
    try:
        origin = event.preferred_origin() or event.origins[0]
    except IndexError:
        raise IndexError("No origin found")
    picked_stations = [pick.waveform_id.station_code for pick in event.picks]
    for network in inventory:
        for station in network:
            if only_picked and station.code in picked_stations:
                # Check that station in already picked
                continue
            for channel in station:
                pick = Pick()
                pick.resource_id = ResourceIdentifier()
                pick.waveform_id = WaveformStreamID(
                    network_code=network.code, station_code=station.code,
                    channel_code=channel.code,
                    location_code=channel.location_code)
                if channel.code[-1] == "Z":
                    pick.phase_hint = "P"
                else:
                    pick.phase_hint = "S"
                depth = origin.depth / 1000.0
                arrivals = model.get_travel_times_geo(
                    source_depth_in_km=depth, 
                    source_latitude_in_deg=origin.latitude,
                    source_longitude_in_deg=origin.longitude,
                    receiver_latitude_in_deg=station.latitude,
                    receiver_longitude_in_deg=station.longitude,
                    phase_list=[pick.phase_hint, pick.phase_hint.lower()])
                try:
                    pick.time = origin.time + arrivals[0].time
                except IndexError:
                    print(arrivals)
                    print("No phase calculated for phase-hint {0} at station "
                        "{1}".format(
                            pick.phase_hint, pick.waveform_id.station_code))
                    continue
                pick.creation_info = CreationInfo(
                    author="ISAP91", agency="VUW")
                event_out.picks.append(pick)
    return event_out


def party_to_tribe(party, data):
    # Group by processing
    process_groups = [Party(families=[party[0]])]
    for family in party[1:]:
        for group in process_groups:
            if family.template.same_processing(group[0].template):
                group.families.append(family)
                break
        else:
            process_groups.append(Party(families=[family]))
    for _party in process_groups:
        length = (
            _party[0].template.st[0].stats.delta * 
            _party[0].template.st[0].stats.npts)
        catalog = Catalog([d.event for f in _party for d in f])
        for event in catalog:
            for pick in event.picks:
                pick.time += _party[0].template.prepick
        tribe = Tribe().construct(
            method="from_meta_file", lowcut=_party[0].template.lowcut,
            highcut=_party[0].template.highcut,
            samp_rate=_party[0].template.samp_rate,
            filt_order=_party[0].template.filt_order,
            prepick=_party[0].template.prepick,
            st=data.copy(), swin='all', meta_file=catalog, 
            length=length, debug=0)
    return tribe

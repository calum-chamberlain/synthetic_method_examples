Data
----

`NZ_resized_DEM.grd` is sourced from [earth-explorer](http://earthexplorer.usgs.gov/)
and is the GMTED2010 dataset.

The `nz-lakes.gmt` file contains water-bodies for New Zealand (the
global lakes in gmt have lakes half-way up mountains) sourced from 
[LINZ]( http://koordinates.com/layer/150-nz-lakes/).

`faults_NZ_WGS84.gmt` contains the New Zealand active fault database:
*Langridge, R.M., Ries, W.F., Litchfield, N.J., Villamor, P., Van Dissen, R.J., Rattenbury,
M.S., Barrell, D.J.A., Heron, D.W., Haubrock, S., Townsend, D.B., Lee, J.A., Cox, S.,
Berryman, K.R., Nicol, A., Stirling, M. (2016). The New Zealand active faults database:
NZAFD250. accepted to New Zealand Journal of Geology and Geophysics 59 (1)*